<?php
    class User extends AppModel {
    public $name = 'User';

    public $validate = array(
        'username' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'A username is required',
                'allowEmpty' => false
            ),
        
        ),
           'first_name' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'A firstname is required',
                'allowEmpty' => false
            ),
        
        ),
             'last_name' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'A lastname is required',
                'allowEmpty' => false
            ),
        
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A password is required'
            ),
            'min_length' => array(
                'rule' => array('minLength', '6'), 
                'message' => 'Password must have a mimimum of 6 characters',
            )
        ),
         
        'confirmpassword' => array(
           'required' => array(
               'rule' => array('notEmpty'),
               'message' => 'Please confirm your password'
           ),
            'equaltofield' => array(
               'rule' => array('equaltofield','password'),
               'message' => 'Both passwords must match.'
           )
        ),
          'newpassword' => array(
           'required' => array(
               'rule' => array('notEmpty'),
               'message' => 'Please  enter new password'
           ),
      
        ),
         
        'email' => array(
            'required' => array(
                'rule' => array('email', true),   
                'message' => 'Please provide a valid email address.'   
            ),
             'unique' => array(
                'rule'    => array('isUniqueEmail'),
                'message' => 'This email is already in use',
            ),
            'between' => array(
                'rule' => array('between', 6, 60),
                'message' => 'Usernames must be between 6 to 60 characters'
            )
        ),
       );

    

 // public $hasMany = 'Attendance';
    public $hasMany = array(
		    'Attendance' => array(
		        'className' => 'Attendance',
		        'foreignKey' => 'user_id'
		    ),
		    'Leave' => array(
		        'className' => 'Leave',
		        'foreignKey' => 'user_id'
		    ),
              'Proceedcheckouts' => array(
                'className' => 'Proceedcheckouts',
                'foreignKey' => 'user_id'
            ),
              'Infraissue' => array(
                'className' => 'Infraissue',
                'foreignKey' => 'user_id'
            ),
                'UserProject' => array(
                'className' => 'UserProject',
                'foreignKey' => 'user_id'
            )
		);
         public function beforeSave($options = array()) {
         if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        // if we get a new password, hash it
        if (isset($this->data[$this->alias]['password_update']) && !empty($this->data[$this->alias]['password_update'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password_update']);
        }     
        // fallback to our parent
        return parent::beforeSave($options);
    }
    
}
?>

