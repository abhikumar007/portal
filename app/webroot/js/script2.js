
function openNav()
 {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("home").style.marginLeft = "250px";
    document.getElementById("home2").style.marginLeft = "250px";
    document.getElementById("project").style.marginLeft = "250px";
    document.getElementById("rock").style.marginLeft = "250px";
     document.getElementById("tab-gallery").style.marginLeft = "250px";
    document.getElementById("client1").style.marginLeft = "250px";
    document.getElementById("client").style.marginLeft = "250px";
    document.getElementById("contact").style.marginLeft = "250px";
    document.getElementById("implink").style.marginLeft = "250px";
    document.getElementById("icon").style.marginLeft= "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
        document.getElementById("home").style.marginLeft = "0";
    document.getElementById("home2").style.marginLeft= "0";
    document.getElementById("project").style.marginLeft= "0";
    document.getElementById("rock").style.marginLeft= "0";
     document.getElementById("tab-gallery").style.marginLeft= "0";
    document.getElementById("client1").style.marginLeft= "0";
    document.getElementById("client").style.marginLeft= "0";
    document.getElementById("contact").style.marginLeft = "0";
    document.getElementById("implink").style.marginLeft = "0";
    document.getElementById("icon").style.marginLeft= "0";
}
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
function myFunction(imgs) {
    var expandImg = document.getElementById("expandedImg");
    var imgText = document.getElementById("imgtext");
    expandImg.src = imgs.src;
    imgText.innerHTML = imgs.alt;
    expandImg.parentElement.style.display = "block";
}
function myFunction(x) {
    x.classList.toggle("fa-thumbs-down");
}
function startTime() {
  var today = new Date();
  var h = today.getHours();
  var m = today.getMinutes();
  var s = today.getSeconds();
  m = checkTime(m);
  s = checkTime(s);
  document.getElementById('txt').innerHTML =
  h + ":" + m + ":" + s;
  var t = setTimeout(startTime, 500);
}
function checkTime(i) {
  if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
  return i;
}