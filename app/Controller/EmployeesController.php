<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class EmployeesController extends AppController {
		var $Helpers = array('Html','Form');
		

/**
 * This controller does not use a model
 *
 * @var array

 */


public $uses = array('proceedcheckout','leave','user','attendance','Admin','UserProject');
public $components = array('Paginator');
public $paginate = array(
	'limit' =>10,
	'order' => array(
		'user.id' => 'desc'
	)
);

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
public function admin_index() {

	$this->set('title','Employees list -Techchzapp Payroll');
	$this->layout= 'Admin/admin';
		if($this->request->is('post')){
		$text = $this->request->data['user']['text'];
		$this->user->recursive = -1;
		$this->Paginator->settings = array(
			'user' => array(
				'conditions'=>array(
					'OR'=>
					array('email'=>$text, 'username'=>$text)
				),
				'limit' =>10,
				'order' => array('user.id' => 'desc')
			)
		);	

		$data = $this->paginate('user');	
		$this->set('data',$data);
	}else
	{
	// $data = $this->user->find('all');
	$this->Paginator->settings = array(
			'user' => array(				
				'limit' =>10,
				'order' => array('user.id' => 'desc')
			)
		);	

		$data = $this->paginate('user');


		foreach($data as $key=>$newdata)	
		{
			
				 // pr($value);

				$data[$key]['totalproject'][] = $this->__projectcount($newdata['user']['id']);
			
		}
		 // pr($data);
		 // die;
	$this->set('data',$data);
}

}

private function __projectcount($user_id)
{
$projects = $this->UserProject->find('count',array('fields' => array('UserProject.project_id','UserProject.user_id'),'conditions'=>array('UserProject.user_id'=>$user_id),'group' => array('UserProject.project_id')));
		// pr($projects);die;
return $projects;
}

public function admin_viewdetails($id=null) {
	if(!isset($id))
{
$this->redirect(array('controller'=>'Employees','action'=>'index','admin'=>true)); 
}
	$this->layout= 'Admin/admin';
	$this->set('title','Employee details -Techchzapp Payroll');
	$data = $this->user->find('first',array('conditions'=>array('User.id'=>$id)));
		// pr($data['user']['username']);die;
	$this->set('data',$data);
}





public function admin_adduser() {
	$this->layout= 'Admin/admin';
	$this->set('title','Add new employee -Techchzapp Payroll');

	if($this->request->is(array('post','put'))){
		/*Upload images*/

		if(!empty($this->request->data['user']['profile_pic']['tmp_name']) && is_uploaded_file($this->request->data['user']['profile_pic']['tmp_name'])){


			$fileName = basename($this->request->data['user']['profile_pic']['name']);
			$filetype = basename($this->request->data['user']['profile_pic']['type']);
			$uploadPath = WWW_ROOT.'img/upload/';
			$uploadFile = $uploadPath.$fileName;
			$ext = explode('.',$fileName);
			$allowExtension = array('png' => 'png','jpg' => 'jpg','jpeg'=>'jpeg');
			if(in_array($ext[1],$allowExtension))
			{
				if(move_uploaded_file($this->request->data['user']['profile_pic']['tmp_name'],$uploadFile)){
					$this->request->data['user']['profile_pic'] = '/img/upload/'.$fileName;
				}
			}
			else
			{
				$this->Flash->successNotification('Failed.', array(
					'key' => 'positive',
				));
			}

		}else{  
			/*upload images end*/
			unset($this->request->data['user']['profile_pic']);
		}



		$this->user->save($this->request->data);
// $log = $this->user->getDataSource()->getLog(false, false);
// debug($log);
// pr($this->request->data);die;
		if($this->user->save($this->request->data)){
			return $this->redirect(array('controller'=>'Employees','action'=>'index','admin'=>true)); 
		}
	}
}


public function admin_editdetails($id=null) {
		if(!isset($id))
{
$this->redirect(array('controller'=>'Employees','action'=>'index','admin'=>true)); 
}
	$this->layout= 'Admin/admin';	 
	$this->set('title','Edit employee details -Techchzapp Payroll');
	if($this->request->is('get')){ 
		$this->request->data =  $this->user->find('first',array('conditions'=>array('User.id'=>$id)));
	}
	if($this->request->is(array('post','put'))){
		if(!empty($this->request->data['user']['profile_pic']['tmp_name']) && is_uploaded_file($this->request->data['user']['profile_pic']['tmp_name'])){


			$fileName = basename($this->request->data['user']['profile_pic']['name']);
			$filetype = basename($this->request->data['user']['profile_pic']['type']);
			$uploadPath = WWW_ROOT.'img/upload/';
			$uploadFile = $uploadPath.$fileName;
			$ext = explode('.',$fileName);
			$allowExtension = array('png' => 'png','jpg' => 'jpg','jpeg'=>'jpeg');
			if(in_array($ext[1],$allowExtension))
			{
				if(move_uploaded_file($this->request->data['user']['profile_pic']['tmp_name'],$uploadFile)){
					$this->request->data['user']['profile_pic'] = '/img/upload/'.$fileName;
				}
			}
			else
			{
				$this->Flash->successNotification('Failed.', array(
					'key' => 'positive',
				));
			}

		}else{  
			/*upload images end*/
			unset($this->request->data['user']['profile_pic']);
		}
		if($this->user->save($this->request->data)){
			return $this->redirect(array('controller'=>'Employees','action'=>'index','admin'=>true)); 
		}
	}
}






public function admin_deletedetails($id) {
	$this->layout= 'Admin/admin';
	
	if($this->user->delete($id))
	{
		return $this->redirect(array('controller'=>'Employees','action'=>'index','admin'=>true));
	}
	die;
}

public function admin_changeStatus()
	{
		$this->layout= false;


		$this->user->id = $_REQUEST['id'];
		if($_REQUEST['data']=='Active'){
			$this->user->saveField('status','0');
			echo "DeActive";die;
		}else{
			$this->user->saveField('status','1');
			echo "Active";die;
		}
		echo 0;die;
	}

public function admin_checkUsername()
	{
		$this->layout= false;

		$username = $_REQUEST['data'];

		$record = $this->user->find('count',array('conditions'=>array('user.username'=>$username)));

		if($record>0){
			
			echo "1";
		}else{
			echo "0";
		}
		die;
	}
}