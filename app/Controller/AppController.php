<?php
/**
* Application level Controller
*
* This file is application-wide controller file. You can put all
* application-wide controller-related methods here.
*
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @package       app.Controller
* @since         CakePHP(tm) v 0.2.9
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/

App::uses('Controller', 'Controller');
/**
* Application Controller
*
* Add your application-wide methods in the class below, your controllers
* will inherit them.
*
* @package		app.Controller
* @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
*/
// app/Controller/AppController.php
class AppController extends Controller {
    public $components = array('Cookie','Session','Auth','RequestHandler');
    public $helpers = array('Cache','Html','Session','Form');


    public function beforeFilter() {
        if ($this->request->prefix == 'admin') {
// Specify which controller/action handles logging in:
AuthComponent::$sessionKey = 'Auth.Admin'; // solution from https://stackoverflow.com/questions/10538159/cakephp-auth-component-with-two-models-session
$this->Auth->loginAction = array('controller'=>'adminlogins','action'=>'login');
$this->Auth->loginRedirect = array('controller'=>'admins','action'=>'index');
$this->Auth->logoutRedirect = array('controller'=>'adminlogins','action'=>'login');
$this->Auth->authenticate = array(
    'Form' => array(
        'userModel' => 'Admin',
    )
);
$this->Auth->allow('admin_login', 'logout','forgetpassword','setpassword');

} else {
AuthComponent::$sessionKey = 'Auth.User'; // solution from https://stackoverflow.com/questions/10538159/cakephp-auth-component-with-two-models-session
$this->Auth->loginAction = array('controller'=>'logins','action'=>'login');
$this->Auth->loginRedirect = array('controller'=>'users','action'=>'index');
$this->Auth->logoutRedirect = array('controller'=>'logins','action'=>'login');
$this->Auth->authenticate = array(
    'Form' => array(
        'userModel' => 'User',
    )
);
// If we get here, it is neither a 'phys' prefixed method, not an 'admin' prefixed method.
// So, just allow access to everyone - or, alternatively, you could deny access - $this->Auth->deny();
$this->Auth->allow('login','homeindex','add', 'logout','forgetpassword','setpassword');           
}
}
public function beforeRender()
{
    $this->loadModel('Leave');
     $this->loadModel('Infraissue');


    $leave = $this->Leave->find( 'count', 
    array(
        'conditions' => array(
            'Leave.status ' => '0'
        )
    ));

  
      $this->set('countNotification',$leave);
      
     
    $Infraissue = $this->Infraissue->find( 'count', 
    array(
        'conditions' => array(
            'Infraissue.status ' => '0'
        )
    ));

  
      $this->set('countNotificationinfraissue',$Infraissue);


       $leave_front = $this->Leave->find( 'count', 
    array(
       'conditions' => array(
            'OR'=>array('AND'=>
                array( 'Leave.status ' => '1'),
                array( 'Leave.status ' => '2'),
                
            ) ,
            array( 'Leave.view_status ' => '0')
            )
    ));

  
      $this->set('countNotificationfront',$leave_front);
      
     
    $Infraissue_fornt = $this->Infraissue->find( 'count', 
    array(
        'conditions' => array(
            'OR'=>array('AND'=>
                array( 'Infraissue.status ' => '1'),
                array( 'Infraissue.status ' => '2')
            ),
            array( 'Infraissue.view_status ' => '0')
                
               
            )
           
        )
    );

      $this->set('countNotificationinfraissuefront',$Infraissue_fornt);


}

public function isAuthorized($user){
// You can have various extra checks in here, if needed.
// We'll just return true though. I'm pretty certain this method has to exist, even if it just returns true.
    return true;
}
}