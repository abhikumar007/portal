<?php
/**
* Static content controller.
*
* This file will render views from views/pages/
*
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @package       app.Controller
* @since         CakePHP(tm) v 0.2.9
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/

App::uses('AppController', 'Controller');

/**
* Static content controller
*
* Override this controller by placing a copy in controllers directory of an application
*
* @package       app.Controller
* @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
*/
class ProfilesController extends AppController {

/**
* This controller does not use a model
*
* @var array
*/
public $uses = array('User');

/**
* Displays a view
*
* @return void
* @throws NotFoundException When the view file could not be found
*	or MissingViewException in debug mode.
*/
public function index() {
  $this->set('title','Payroll-Profile');
  $this->layout= 'homepage';


  if($this->request->is('get')){
   $this->request->data =  $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('User.id'))));
 }

 elseif($this->request->is(array('post','put'))){

  /*Update Password*/

  $oldPassword = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('User.id')),'fields'=>array('User.id','User.password')));
  $newpassword= $this->request->data['passwordupdate']['newpassword'];
  $confirmpassword= $this->request->data['passwordupdate']['confirmpassword'];
  if($oldPassword['User']['password']==AuthComponent::password($this->request->data['passwordupdate']['oldpassword']))
  {

    if($newpassword == $confirmpassword)
    {

      $this->User->id = $oldPassword['User']['id'];

      if($this->User->saveField('password',$confirmpassword)){
        // $this->Flash->successNotification('Request updated succesfully.', array(
        //   'key' => 'positive',
        // ));
        $this->redirect('index');

      }
    }
  }


  /*Update password end*/

  /*Upload images*/

  if(!empty($this->request->data['User']['profile_pic']['tmp_name']) && is_uploaded_file($this->request->data['User']['profile_pic']['tmp_name'])){


    $fileName = basename($this->request->data['User']['profile_pic']['name']);
    $filetype = basename($this->request->data['User']['profile_pic']['type']);
    $uploadPath = WWW_ROOT.'img/upload/';
    $uploadFile = $uploadPath.$fileName;
    $ext = explode('.',$fileName);
    $allowExtension = array('png' => 'png','jpg' => 'jpg','jpeg'=>'jpeg');
    if(in_array($ext[1],$allowExtension))
    {
     if(move_uploaded_file($this->request->data['User']['profile_pic']['tmp_name'],$uploadFile)){
       $this->request->data['User']['profile_pic'] = '/img/upload/'.$fileName;
     }
   }
   else
   {
    $this->Flash->successNotification('Failed.', array(
      'key' => 'positive',
    ));
  }

}else{  
  /*upload images end*/
  unset($this->request->data['User']['profile_pic']);
}

$this->User->set($this->request->data);

if($this->User->validates()){ 

  if($this->User->save($this->request->data)){

    $this->redirect('index');

  }

  return $this->redirect(array('controller'=>'profile','action'=>'INDEX'));
}

        // $this->request->data =  $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('User.id'))));


}
}
}
