<?php
/**
* Static content controller.
*
* This file will render views from views/pages/
*
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @package       app.Controller
* @since         CakePHP(tm) v 0.2.9
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/

App::uses('AppController', 'Controller');

/**
* Static content controller
*
* Override this controller by placing a copy in controllers directory of an application
*
* @package       app.Controller
* @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
*/
class InfraissuesController extends AppController {
	var $Helpers = array('Html','Form');

/**
* This controller does not use a model
*
* @var array
*/
public $uses = array('infraissue');
public $components = array('Paginator');
public $paginate = array(
	'limit' => 10,
	'order' => array(
		'infraissue.id' => 'desc'
	)
);

/**
* Displays a view
*
* @return void
* @throws NotFoundException When the view file could not be found
*	or MissingViewException in debug mode.
*/
public function index() {
	$this->set('title','Infra issues');
	$this->layout= 'homepage';
	if($this->request->is('post')){
		$month = $this->request->data['infraissue']['month'];
		$year = $this->request->data['infraissue']['year'];
		$this->Paginator->settings = array(
			'infraissue' => array(
				'conditions'=>array('MONTH(date)'=>$month,'YEAR(date)'=>$year,'infraissue.user_id'=>$this->Auth->user('User.id')),
				'limit' =>10,
				'order' => array('id' => 'desc')
			)
		);	
		$data = $this->paginate('infraissue');	
		$this->set('row',$data);
	}else
	{
		$this->Paginator->settings = array(
			'infraissue' => array(
				'conditions'=>array('infraissue.user_id'=>$this->Auth->user('User.id')),
				'limit' => 10,
				'order' => array('id' => 'desc')
			)
		);	
		$data = $this->paginate('infraissue');
		$this->set('row',$data);
	}
}

public function newinfraissue () {
	$this->set('title','New infra issues');
	$this->layout= 'homepage';
	$this->request->data['infraissue']['user_id']=$this->Auth->user('User.id');
	if($this->request->is('post')){
		$this->request->data['infraissue']['date'] = date('Y-m-d', strtotime($this->request->data['infraissue']['date']));
		if($this->infraissue->save($this->request->data))
		{
			return $this->redirect('newinfraissue');
		}
	}	
	$this->Paginator->settings = array(
		'infraissue' => array(
			'conditions'=>array('infraissue.user_id'=>$this->Auth->user('User.id')),
			'limit' => 10,
			'order' => array('id' => 'desc')
		)
	);	
	$data = $this->paginate('infraissue');
	$this->set('row',$data);
}


	 public function notifications() {
    $this->layout='homepage';
    $this->set('title','Infraissue notifications list');
    // $infraissue = $this->infraissue->find('all', array('conditions'=>array('infraissue.user_id'=>$this->Auth->user('User.id'), 'infraissue.view_status'=>'0')));
  
    // pr($infraissue);die;


    $this->Paginator->settings = array(
            'infraissue' => array(
                'conditions'=>array('infraissue.user_id'=>$this->Auth->user('User.id'), 'infraissue.view_status'=>'0'),     
                'limit' =>10,
                'order' => array('infraissue.id' => 'desc'),
                 'group' => '`infraissue`.`user_id`'
            )
        );  
      $this->infraissue->updateAll(
        array('infraissue.view_status'=>'1'),
        array('infraissue.user_id'=> $this->Auth->user('User.id'))
    );
        $data = $this->paginate('infraissue');   
        // pr($data);die;
        $this->set('data',$data);
        $this->redirect(array('controller'=>'infraissues','action'=>'index'));

    }


}
