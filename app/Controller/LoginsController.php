<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class LoginsController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
public $uses = array('User','Attendance');

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */

public function login() {

	$this->set('title','login');
	$this->layout= 'login';
	if($this->request->is('post'))
	{

			$this->User->set($this->request->data);

			if($this->User->validates()){ 
				$loginUser = $this->User->find('first',array('conditions'=>array('User.username'=>$this->request->data['login']['username'],'User.password'=>AuthComponent::password($this->request->data['login']['password']),'User.status'=>1)));
				
				if (isset($loginUser) && !empty($loginUser)) {
					if ($this->Auth->login($loginUser)) {


						$alreadyLoggedIn = $this->Attendance->find('count',array('conditions'=>array('date_format(Attendance.checkin,"%Y-%m-%d")'=>date('Y-m-d'))));
// 						$log = $this->Attendance->getDataSource()->getLog(false, false);
// debug($log);die;
						
						if($alreadyLoggedIn == 1)
						{
							return $this->redirect($this->Auth->redirectUrl());
						}
						else
						{
								$checkIn['Attendance']['checkin'] = date("Y-m-d H:i:s");
							$checkIn['Attendance']['checkout'] = date("Y-m-d H:i:s");
							$checkIn['Attendance']['user_id'] = $loginUser['User']['id'];

							if($this->Attendance->save($checkIn)){
								
								return $this->redirect($this->Auth->redirectUrl());
								
							}else{
								die('something went wrong');
							}
						}
						
						
							return $this->redirect($this->Auth->redirectUrl());
					}					
				}
				
						return $this->redirect(array('controller'=>'logins','action'=>'login'));
			}
		}
	}



public function beforeFilter() {
	parent::beforeFilter();
    // Allow users to register and logout.
}

public function logout() {
	
	$user_id =  $this->Auth->user('User.id');
	$attendence_user = $this->Attendance->find('first', array('conditions' => array('Attendance.user_id' => $user_id), 'fields'=>array('Attendance.id', 'Attendance.user_id','Attendance.checkin'),'order' => array('Attendance.id' => 'DESC') ));

	if(isset($attendence_user) && !empty( $attendence_user)){
		$this->Attendance->id = $attendence_user['Attendance']['id'];echo '<br>';

		echo $checkin = $attendence_user['Attendance']['checkin'];echo '<br>';

		 echo $checkout = date("Y-m-d H:i:s");echo '<br>';

		 	$start_date = new DateTime(date("Y-m-d H:i:s"));
			$since_start = $start_date->diff(new DateTime($attendence_user['Attendance']['checkin']));
			echo $since_start->days.' days total<br>';
			echo $since_start->y.' years<br>';
			echo $since_start->m.' months<br>';
			echo $since_start->d.' days<br>';
			echo $since_start->h.' hours<br>';
			echo $since_start->i.' minutes<br>';
			echo $since_start->s.' seconds<br>';

			echo $hour = $since_start->h.":".$since_start->i.":".$since_start->s;

// 		echo $hour = strtotime($checkout) - $checkin;echo '<br>';
// 		pr($hour);
// echo date("Y-m-d H:i:s",$hour);

		// die;

 
		if($this->Attendance->saveField('checkout',$checkout) && $this->Attendance->saveField('hour',$hour)){
			$this->Session->setFlash('Successfully Logged Out');
    		$this->redirect($this->Auth->logout());
		}
	}
die;
}


public function add() {

       
	$this->request->data['User']['username'] = '';
	$this->request->data['User']['first_name'] = '';
	$this->request->data['User']['last_name'] = '';
	$this->request->data['User']['email'] = '';
	$this->request->data['User']['password'] = '';
	$this->request->data['User']['profile_pic'] = '';

	$this->User->create();
	if ($this->User->save($this->request->data)) {
		$this->Flash->success(__('The user has been saved'));
		return $this->redirect(array('action' => 'index'));
	}
	$this->Flash->error(
		__('The user could not be saved. Please, try again.')
	);
        
}

public function forgetpassword() {
	$this->set('title','Forget password');
	$this->layout= 'login';
	echo $key = md5(microtime().rand());
	if($this->request->is('post'))
	{
		// pr($this->request->data['forgetpassword']['email']);
		$data =$this->User->find('first',array('conditions'=>array('User.email'=>$this->request->data['forgetpassword']['email'])));
		
	 if(empty($data['User']['id']))
	 {
	 	 $this->Flash->successNotification('This account doesnot exists.', array('key' => 'positive',));
	 		 $this->redirect(array('controller'=>'Logins','action'=>'forgetpassword'));
	 	


	 }
	 $this->redirect(array('controller'=>'Logins','action'=>'setpassword',$data['User']['id'],$key));
	}
}



public function setpassword($id) {
	$this->set('title','Set password');
	$this->layout= 'login';
	
	if($this->request->is('post')){
 	$data1 = $this->User->find('first',array('conditions'=>array('User.id'=>$id)));
 	if($data1['User']['id'] == $id)
 	{

		 
			$newpassword= $this->request->data['setpassword']['newpassword'];
   			 $confirmpassword = $this->request->data['setpassword']['confirmpassword'];
   			 $passvalue = $this->User->password =  $confirmpassword; 
   			 // echo $passvalue;die;
         	$this->User->id=$id;
         	
              if($this->User->saveField('password',$passvalue)){

                // $this->Flash->successNotification('Request updated succesfully.', array(
                // 'key' => 'positive',
                // ));
                $this->redirect('login ');
            }

             }
         


	}
	

}

}

