<?php
/**
* Static content controller.
*
* This file will render views from views/pages/
*
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @package       app.Controller
* @since         CakePHP(tm) v 0.2.9
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/

App::uses('AppController', 'Controller');

/**
* Static content controller
*
* Override this controller by placing a copy in controllers directory of an application
*
* @package       app.Controller
* @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
*/
class AttendancesController extends AppController {
	var $Helpers = array('Html','Form');

/**
* This controller does not use a model
*
* @var array
*/
public $uses = array('attendance');
public $components = array('Paginator');
public $paginate = array(
	'limit' =>10,
	'order' => array(
		'attendance.id' => 'desc'
	)
);

/**
* Displays a view
*
* @return void
* @throws NotFoundException When the view file could not be found
*	or MissingViewException in debug mode.
*/
public function index() {
	$this->set('title','Attendance');
	$this->layout= 'homepage';
	if($this->request->is('post')){
		$month = $this->request->data['attendance']['month'];
		$year = $this->request->data['attendance']['year'];
		$this->attendance->recursive = -1;
		$this->Paginator->settings = array(
			'attendance' => array(
				'conditions'=>array('MONTH(checkin)'=>$month,'YEAR(checkin)'=>$year,'attendance.user_id'=>$this->Auth->user('User.id')),
				'limit' =>10,
				'order' => array('attendance.id' => 'desc')
			)
		);	
		$data = $this->paginate('attendance');	
		$this->set('row',$data);
	}
	else{
		$this->Paginator->settings = array(
			'attendance' => array(
				'conditions'=>array('attendance.user_id'=>$this->Auth->user('User.id')),
				'limit' =>10,
				'order' => array('attendance.id' => 'desc')
			)
		);	
		$data = $this->paginate('attendance');	

		$this->set('row',$data);
	}
}


}