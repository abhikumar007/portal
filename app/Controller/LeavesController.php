<?php
/**
* Static content controller.
*
* This file will render views from views/pages/
*
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @package       app.Controller
* @since         CakePHP(tm) v 0.2.9
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/

App::uses('AppController', 'Controller');

/**
* Static content controller
*
* Override this controller by placing a copy in controllers directory of an application
*
* @package       app.Controller
* @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
*/
class LeavesController extends AppController {
    var $Helpers = array('Html','Form');

/**
* This controller does not use a model
*
* @var array
*/
public $uses = array('leave','users','Notification');
public $components = array('Paginator');
public $paginate = array(
    'limit' =>10,
    'order' => array(
        'leave.id' => 'desc'
    )
);


/**
* Displays a view
*
* @return void
* @throws NotFoundException When the view file could not be found
*  or MissingViewException in debug mode.
*/
public function index() {
    $this->set('title','Leave list');
    $this->layout= 'homepage';
    if($this->request->is('post')){
        $month = $this->request->data['leave']['month'];
        $year = $this->request->data['leave']['year'];
        $this->Paginator->settings = array(
            'leave' => array(
                'conditions'=>array('MONTH(start_date)'=>$month,'YEAR(start_date)'=>$year,'leave.user_id'=>$this->Auth->user('User.id')),
                'limit' =>10,
                'order' => array('id' => 'desc')
            )
        );  
        $data = $this->paginate('leave');  
        $this->set('row',$data);
    }
    else{
        $this->Paginator->settings = array(
            'leave' => array(
                'conditions'=>array('leave.user_id'=>$this->Auth->user('User.id')),
                'limit' =>10,
                'order' => array('id' => 'desc')
            )
        );  
        $data = $this->paginate('leave');   
        $this->set('row',$data);
    }
}


public function newleaverqst($id=null) {
    $this->set('title','newleaverqst');
    $this->layout= 'homepage';
    if($this->request->is('post')){
        $this->request->data['leave']['user_id']=$this->Auth->user('User.id');
        $this->request->data['leave']['start_date'] = date('Y-m-d', strtotime($this->request->data['leave']['start_date']));
        $this->request->data['leave']['end_date'] = date('Y-m-d', strtotime($this->request->data['leave']['start_date']. '+ '.$this->request->data['leave']['days'].'days'));
        $this->leave->save($this->request->data);
        return $this->redirect('newleaverqst');
    }
    $data = $this->users->find('all',array('conditions'=>array('users.id'=>$this->Auth->user('User.id'))));
    $this->set('row',$data);
}

public function deleteleave($id)
{
    $this->layout= false;
    if($this->leave->delete($id))
    {
        $this->redirect('index');
    }
}

    public function notifications() {
    $this->layout='homepage';
    $this->set('title','Leave notifications list');
    // $leaves = $this->leave->find('all', array('conditions'=>array('leave.user_id'=>$this->Auth->user('User.id'), 'leave.view_status'=>'0')));
  
    // pr($leaves);die;


    $this->Paginator->settings = array(
            'leave' => array(
                'conditions'=>array('leave.user_id'=>$this->Auth->user('User.id'), 'leave.view_status'=>'0'),       
                'limit' =>10,
                'order' => array('leave.id' => 'desc'),
                 'group' => '`leave`.`user_id`'
            )
        );  
      $this->leave->updateAll(
        array('leave.view_status'=>'1'),
        array('leave.user_id'=> $this->Auth->user('User.id'))
    );
        $data = $this->paginate('leave');   
        // pr($data);die;
        $this->set('data',$data);

    }

}