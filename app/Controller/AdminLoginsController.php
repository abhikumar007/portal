<?php
/**
* Static content controller.
*
* This file will render views from views/pages/
*
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @package       app.Controller
* @since         CakePHP(tm) v 0.2.9
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/

App::uses('AppController', 'Controller');

/**
* Static content controller
*
* Override this controller by placing a copy in controllers directory of an application
*
* @package       app.Controller
* @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
*/
class AdminLoginsController extends AppController {

/**
* This controller does not use a model
*
* @var array
*/
public $uses = array('Admin');

/**
* Displays a view
*
* @return void
* @throws NotFoundException When the view file could not be found
*	or MissingViewException in debug mode.
*/

public function admin_login() {
	$this->layout='Admin\admin_login';
	$this->set('title','Login');
	if($this->request->is('POST'))
	{
		$this->Admin->set($this->request->data);
		$loginAdmin = $this->Admin->find('first',array('conditions'=>array('Admin.username'=>$this->request->data['Admin']['username'],'Admin.password'=>AuthComponent::password($this->request->data['Admin']['password']),'Admin.status'=>1)));

		if (isset($loginAdmin) && !empty($loginAdmin)) {
			if ($this->Auth->login($loginAdmin)) {
//$this->Flash->successNotification('Logged-in Successfully.', array('key' => 'positive'));
				return $this->redirect($this->Auth->redirectUrl());
			}
		}elseif($alreadyLoggedIn == 1)
		{
			return $this->redirect($this->Auth->redirectUrl());
		}
		else
		{
//$this->Flash->failureNotification('Invalid Adminname or password, try again.', array('key' => 'positive'));
			return $this->redirect(array('controller'=>'AdminLogins','action'=>'login','admin'=>true));
		}
	}
}


/*admin forgot password start*/


public function admin_forgotpassword() {
	$this->layout='Admin\admin_login';
	$this->set('title','Login');
	if($this->request->is('post'))
	{
		$data =$this->Admin->find('first',array('conditions'=>array('Admin.email'=>$this->request->data['Adminforgetpassword']['email'])));

		if(empty($data['Admin']['id']))
		{
			$this->Flash->successNotification('This account doesnot exists.', array('key' => 'positive',));
			$this->redirect(array('controller'=>'AdminLogins','action'=>'forgotpassword'));		


		}
		$this->redirect(array('controller'=>'AdminLogins','action'=>'setpassword',$data['Admin']['id']));
	}
}

/*admin forgot password end*/



/*admin set password start*/


public function admin_setpassword($id) {
	$this->layout='Admin\admin_login';
	$this->set('title','setpassword');
	
	if($this->request->is('post')){
 	$data1 = $this->Admin->find('first',array('conditions'=>array('Admin.id'=>$id)));
 	if($data1['Admin']['id'] == $id)
 	{

		 
			$newpassword= $this->request->data['Adminsetpassword']['newpassword'];
   			 $confirmpassword = $this->request->data['Adminsetpassword']['confirmpassword'];
   			 $passvalue = $this->Admin->password =  $confirmpassword; 
   			 // echo $passvalue;die;
         	$this->Admin->id=$id;
         	
              if($this->Admin->saveField('password', AuthComponent::password($passvalue))){

                // $this->Flash->successNotification('Request updated succesfully.', array(
                // 'key' => 'positive',
                // ));
                $this->redirect('login ');
            }

             }
         


	}
	

}
// /*admin set password end*/

public function beforeFilter() {
	parent::beforeFilter();
}

}

