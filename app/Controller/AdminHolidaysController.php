<?php
/**
* Static content controller.
*
* This file will render views from views/pages/
*
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @package       app.Controller
* @since         CakePHP(tm) v 0.2.9
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/

App::uses('AppController', 'Controller');

/**
* Static content controller
*
* Override this controller by placing a copy in controllers directory of an application
*
* @package       app.Controller
* @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
*/
class AdminHolidaysController extends AppController {
	var $Helpers = array('Html','Form');

/**
* This controller does not use a model
*
* @var array
*/
public $uses = array('holiday');
public $components = array('Paginator');
public $paginate = array(
	'limit' =>10,
	'order' => array(
		'holiday.id' => 'desc'
	)
);

/**
* Displays a view
*
* @return void
* @throws NotFoundException When the view file could not be found
*	or MissingViewException in debug mode.
*/
public function admin_index() {

	$this->set('title','Holidays List');
	$this->layout= 'admin/admin';
	if($this->request->is('post')){
		$month = $this->request->data['holiday']['month'];
// $year = $this->request->data['holiday']['year'];
		$this->holiday->recursive = -1;
		$this->Paginator->settings = array(
			'holiday' => array(
				'conditions'=>array('MONTH(date)'=>$month),
				'limit' =>10,
				'order' => array(
					'holiday.id' => 'desc'
				)
			)
		);	

		$data = $this->paginate('holiday');	
		$this->set('data',$data);
	}else
	{
		$this->Paginator->settings = array(
			'holiday' => array(

				'limit' =>10,
				'order' => array(
					'holiday.id' => 'desc'
				)
			)
		);	

		$data = $this->paginate('holiday');	
		$this->set('data',$data);
	}
}

public function admin_addholiday() {

	$this->set('title','Add new holidays');
	$this->layout= 'admin/admin';
	if($this->request->is('post'))
	{
		$this->request->data['holiday']['date'] = date('Y-m-d', strtotime($this->request->data['holiday']['date']));

		if($this->holiday->save($this->request->data))
		{
			return $this->redirect(array('controller'=>'AdminHolidays','action'=>'index','admin'=>true)); 
		}
	}
}
public function admin_editholiday($id) {
	$this->layout= 'Admin/admin';
	$this->set('title','Edit holidays');
	if($this->request->is('get')){ 
		$this->request->data =  $this->holiday->find('first',array('conditions'=>array('holiday.id'=>$id)));

	}
	if($this->request->is(array('post','put'))){
		$this->request->data['holiday']['date'] = date('Y-m-d', strtotime($this->request->data['holiday']['date']));
		if($this->holiday->save($this->request->data)){
			return $this->redirect(array('controller'=>'AdminHolidays','action'=>'index','admin'=>true)); 
		}
	}
}

public function admin_deleteholiday($id) {
	$this->layout= 'Admin/admin';
	if($this->holiday->delete($id))
	{
		return $this->redirect(array('controller'=>'AdminHolidays','action'=>'index','admin'=>true));
	}
	die;
}
}