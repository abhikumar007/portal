<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class AdminLeavesController extends AppController {
		var $Helpers = array('Html','Form');

/**
 * This controller does not use a model
 *
 * @var array
 */
public $uses = array('User','Leave','Attendance');
public $components = array('Paginator');
public $paginate = array(
	'limit' =>10,
	'order' => array(
		'User.id' => 'desc'
	)
);

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */

public function admin_index() {
	$this->layout='Admin\admin';
	$this->set('title','Employees list');
	if($this->request->is('post')){
		$text = $this->request->data['User']['text'];
		$this->User->recursive = -1;
		$this->Paginator->settings = array(
			'User' => array(
				'conditions'=>array(
					'OR'=>
					array('email'=>$text, 'username'=>$text)
				),
				'limit' =>10,
				'order' => array('User.id' => 'desc')
			)
		);	

		$data = $this->paginate('User');	


		$this->set('data',$data);
	}else
	{
	// $data = $this->user->find('all');
		$this->Paginator->settings = array(
			'User' => array(
				
				'limit' =>10,
				'order' => array('User.id' => 'desc')
			)
		);	

		$data = $this->paginate('User');	
		$this->set('data',$data);
	}
	// $data = $this->User->find('all');
	// $this->set('row',$data);
}


public function admin_viewleave($id=null) {
if(!isset($id))
{
$this->redirect(array('controller'=>'AdminLeaves','action'=>'index','admin'=>true)); 
}
	$this->set('title','Employees leave list');
	$this->layout='Admin\admin';
if($this->request->is('post')){
$month = $this->request->data['Leave']['month'];
$year = $this->request->data['Leave']['year'];
$this->User->unbindModel(array('hasMany'=>array('Leave')), true);
$this->Leave->recursive = -1;
$this->Paginator->settings = array(
'Leave' => array(
'conditions'=>array('MONTH(start_date)'=>$month,'YEAR(start_date)'=>$year,'Leave.user_id'=>$id),
'limit' =>10,
'order' => array(
'Leave.id' => 'desc'
)
)
);	
$data = $this->paginate('Leave');	
$this->set('data',$data);
}
else{
$this->Paginator->settings = array(
'Leave' => array(
'conditions'=>array('Leave.user_id'=>$id),
'limit' =>10,
'order' => array('Leave.id' => 'desc')
)
);	

$data = $this->paginate('Leave');	
$this->set('data',$data);
}
$data = $this->User->find('first',array('conditions'=>array('User.id'=>$id)));
$this->set('row',$data);
}	

public function beforeFilter() {
	parent::beforeFilter();
}


public function admin_leaverequestaction($id=null,$user_id=null,$status=null)
	{
		if(!isset($id))
{
$this->redirect(array('controller'=>'AdminLeaves','action'=>'index','admin'=>true)); 
}
		$this->layout='Admin\admin';
		$leave_user = $this->User->find('first',array('conditions'=>array('User.id'=>$user_id)));
		
		$leave_request = $this->Leave->find('first',array('conditions'=>array('Leave.id'=>$id)));
			 //pr($leave_request);die;
		if($leave_user['User']['totalleaves'] >= $leave_user['User']['leaveleft'] && $leave_user['User']['leaveleft'] >= $leave_request['Leave']['days'])
		{
			
		if($status == 'Accept')
			{
				$var = 1;
				$data['Leave']['status'] = $var;
			
				
				

				$this->Leave->id=$id;
				if($this->Leave->saveField('status',$var)){
					$newleaveleft =  $leave_user['User']['leaveleft'] - $leave_request['Leave']['days'];
					$newleaveavailed =  $leave_user['User']['leaveavailed'] + $leave_request['Leave']['days'];
					$this->User->id=$user_id;
					$this->User->updateAll(
						    array('User.leaveleft' => $newleaveleft,'User.leaveavailed' =>$newleaveavailed),
						 
						    array('User.id' =>$user_id)
						);
					
					$this->redirect($this->referer());
					
				}

			}elseif($status=='Decline')
			{
				$this->redirect(array('controller'=>'AdminLeaves','action'=>'addcomment','admin'=>true)); 
			// 	$var=2;
			// 	$data['Leave']['status'] = $var;
			// 		$this->Leave->id=$id;
			// 	if($this->Leave->saveField('status',$var)){
			// 	$this->redirect($this->referer());
			// }
			}
			else
			{
				$var=0;
				$this->redirect($this->referer());
			}			

		}else
		{
			$this->redirect($this->referer());
		}

	}


	public function admin_notifications() {
	$this->layout='Admin\admin';
	$this->set('title','Leave notifications list');
	$this->Paginator->settings = array(
			'Leave' => array(
				'fields'=>array('Leave.*','COUNT(`Leave`.`user_id`) as `entity_count`'),
				'conditions'=>array('Leave.status'=>'0'),				
				'limit' =>10,
				'order' => array('Leave.id' => 'desc'),
				 'group' => '`Leave`.`user_id`'
			)
		);	
		$data = $this->paginate('Leave');	
		// pr($data);die;
		$this->set('data',$data);

	}
	public function admin_manageleaverequest()
	{
			$this->layout='Admin\admin';
	$this->set('title','Leave Requests');
	$this->Paginator->settings = array(
			'Leave' => array(
				'fields'=>array('Leave.*','COUNT(`Leave`.`user_id`) as `entity_count`'),
				'conditions'=>array('Leave.status'=>'0'),				
				'limit' =>10,
				'order' => array('Leave.date' => 'desc'),
				 'group' => '`Leave`.`user_id`'
			)
		);	
		$data = $this->paginate('Leave');	
		// pr($data);die;
		$this->set('data',$data);
	}



  public function admin_addcomment($id=null) {
	if(!isset($id))
	{
		$this->redirect(array('controller'=>'AdminLeaves','action'=>'index','admin'=>true)); 
	}
	$this->set('title','Add comment');
	$this->layout='Admin\admin';
	$data =$this->Leave->find('first',array('condition'=>array('Leave.id'=>$id)));
	$this->request->data['Leave']['status']=2;
	if($this->request->is('post'))
		{
			$this->Leave->id=$id;
			if($this->Leave->save($this->request->data))
			{

				$this->redirect(array('controller'=>'AdminLeaves','action'=>'index','admin'=>true)); 
			}
			else
			{
				echo"0";
			}
		}
	}
}