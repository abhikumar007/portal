<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class AdminProjectreportsController extends AppController {
	var $Helpers = array('Html','Form');

/**
 * This controller does not use a model
 *
 * @var array
 */
public $uses = array('User','Attendance','Proceedcheckout');
public $components = array('Paginator');
public $paginate = array(
	'limit' =>10,
	'order' => array(
		'User.id' => 'desc'
	)
);

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */

public function admin_index() {
	$this->layout='Admin\admin';
	$this->set('title','Project reports');

	if($this->request->is('post')){
		$text = $this->request->data['User']['text'];
		$this->User->recursive = -1;
		$this->Paginator->settings = array(
			'User' => array(
				'conditions'=>array(
					'OR'=>
					array('email'=>$text, 'username'=>$text)
				),
				'limit' =>10,
				'order' => array('User.id' => 'desc')
			)
		);	

		$data = $this->paginate('User');	


		$this->set('data',$data);
	}else
	{
	// $data = $this->user->find('all');
		$this->Paginator->settings = array(
			'User' => array(
				
				'limit' =>10,
				'order' => array('User.id' => 'desc')
			)
		);	

		$data = $this->paginate('User');	
		$this->set('data',$data);
	}
	// $data = $this->User->find('all');
	// $this->set('row',$data);
}


public function admin_viewreport($id=null) {
	if(!isset($id))
{
$this->redirect(array('controller'=>'AdminProjectreports','action'=>'index','admin'=>true)); 
}
	$this->layout='Admin\admin';
	$this->set('title','View project report');
	if($this->request->is('post')){
		$month = $this->request->data['Proceedcheckout']['month'];
		$year = $this->request->data['Proceedcheckout']['year'];
		$this->User->unbindModel(array('hasMany'=>array('Proceedcheckout')), true);
		$this->Proceedcheckout->recursive = -1;
		$this->Paginator->settings = array(
		'Proceedcheckout' => array(
		'conditions'=>array('MONTH(date)'=>$month,'YEAR(date)'=>$year,'Proceedcheckout.user_id'=>$id),
		'limit' =>10,
		'order' => array(
		'Leave.id' => 'desc'
		)
		)
		);	
		$data = $this->paginate('Proceedcheckout');	
		$this->set('data',$data);
		}
		else{
		$this->Paginator->settings = array(
		'Proceedcheckout' => array(
			'conditions'=>array('Proceedcheckout.user_id'=>$id),
		'limit' =>10,
		'order' => array('Proceedcheckout.id' => 'desc')
		)
		);	

		$data = $this->paginate('Proceedcheckout');	
		$this->set('data',$data);
		}
		$data = $this->User->find('first',array('conditions'=>array('User.id'=>$id)));
		$this->set('row',$data);	
}	

public function beforeFilter() {
	parent::beforeFilter();
    // Allow users to register and logout.
}

}

