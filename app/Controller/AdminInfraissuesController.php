<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class AdminInfraissuesController extends AppController {
		var $Helpers = array('Html','Form');

/**
 * This controller does not use a model
 *
 * @var array
 */
public $uses = array('User','Infraissue');
public $components = array('Paginator');
public $paginate = array(
	'limit' =>10,
	'order' => array(
		'User.id' => 'desc'
	)
);


/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */

public function admin_index() {
	$this->layout='Admin\admin';
	$this->set('title','View employee list');
		if($this->request->is('post')){
	$text = $this->request->data['User']['text'];
		$this->User->recursive = -1;
		$this->Paginator->settings = array(
			'User' => array(
			'conditions'=>array(
					'OR'=>
					array('email'=>$text, 'username'=>$text)
				),
				'limit' =>10,
				'order' => array('User.id' => 'desc')
			)
		);	

		$data = $this->paginate('User');	


		$this->set('data',$data);
	}else
	{
	// $data = $this->user->find('all');
		$this->Paginator->settings = array(
			'User' => array(
				
				'limit' =>10,
				'order' => array('User.id' => 'desc')
			)
		);	

		$data = $this->paginate('User');	
		$this->set('data',$data);
	}
}



public function admin_viewinfraissue($id=null) {
		if(!isset($id))
	{
		 $this->redirect(array('controller'=>'AdminInfraissues','action'=>'index','admin'=>true)); 
	}
	$this->layout='Admin\admin';
	$this->set('title','View infraissue');
		if($this->request->is('post')){
		$month = $this->request->data['Infraissue']['month'];
		$year = $this->request->data['Infraissue']['year'];
		$this->User->unbindModel(array('hasMany'=>array('Infraissue')), true);
		$this->Infraissue->recursive = -1;
		$this->Paginator->settings = array(
		'Infraissue' => array(
		'conditions'=>array('MONTH(date)'=>$month,'YEAR(date)'=>$year,'Infraissue.user_id'=>$id),
		'limit' =>10,
		'order' => array(
		'Infraissue.id' => 'desc'
		)
		)
		);	
		$data = $this->paginate('Infraissue');	
		$this->set('data',$data);
		}
		else{
		$this->Paginator->settings = array(
		'Infraissue' => array(
		'conditions'=>array('Infraissue.user_id'=>$id),
		'limit' =>10,
		'order' => array('Infraissue.id' => 'desc')
		)
		);	

		$data = $this->paginate('Infraissue');	
		$this->set('data',$data);
		}
		$data = $this->User->find('first',array('conditions'=>array('User.id'=>$id)));
		$this->set('row',$data);
	
}	

public function admin_addcomment($id=null) {
			if(!isset($id))
	{
		 $this->redirect(array('controller'=>'AdminInfraissues','action'=>'index','admin'=>true)); 
	}
	$this->layout='Admin\admin';
	$this->set('title','Infraissue Add comment');

	$data =$this->Infraissue->find('first',array('condition'=>array('Infraissue.id'=>$id)));
	
	if($this->request->is('post'))
	{
        $this->Infraissue->id=$id;
		if($this->Infraissue->save($this->request->data))
		{
			
			return $this->redirect('index');
			
		}
		else
		{
			echo"0";
		}
	}	
}
public function beforeFilter() {
	parent::beforeFilter();
}
	public function admin_notifications() {
	$this->layout='Admin\admin';
	$this->set('title','Infraissue notifications list');
	$this->Paginator->settings = array(
			'Infraissue' => array(
				'fields'=>array('Infraissue.*','COUNT(`Infraissue`.`user_id`) as `entity_count`'),
				'conditions'=>array('Infraissue.status'=>'0'),				
				'limit' =>10,
				'order' => array('Infraissue.id' => 'desc'),
				 'group' => '`Infraissue`.`user_id`'
			)
		);	
		$data = $this->paginate('Infraissue');	
		// pr($data);die;
		$this->set('data',$data);

	}
}

