<?php
/**
* Static content controller.
*
* This file will render views from views/pages/
*
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @package       app.Controller
* @since         CakePHP(tm) v 0.2.9
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/

App::uses('AppController', 'Controller');

/**
* Static content controller
*
* Override this controller by placing a copy in controllers directory of an application
*
* @package       app.Controller
* @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
*/
class AdminProjectsController extends AppController {
	var $Helpers = array('Html','Form');

/**
* This controller does not use a model
*
* @var array
*/
public $uses = array('Projects','Users','UserProject');
public $components = array('Paginator');
public $paginate = array(
	'limit' =>10,
	'order' => array(
		'Projects.id' => 'desc'
	)
);

/**
* Displays a view
*
* @return void
* @throws NotFoundException When the view file could not be found
*	or MissingViewException in debug mode.
*/
public function admin_index() {
	$this->set('title','projects List');
	$this->layout= 'admin/admin';
	$data = $this->paginate('Projects');	
	$this->set('data',$data);
}
public function admin_viewprojectdetails($id=null) {
	if(!isset($id))
	{
		$this->redirect(array('controller'=>'AdminProjects','action'=>'index','admin'=>true)); 
	}
	$this->layout= 'Admin/admin';
	$this->set('title','Projects detail -Techchzapp Payroll');
	$this->Projects->bindModel(array(
		'hasMany' => array(
			'UserProject' => array(
				'foreignKey' => false,
				'conditions' => array('UserProject.project_id = '.$id)
			)
		)
	)
);
	$data = $this->Projects->find('first',array('conditions'=>array('Projects.id'=>$id),'recursive'=>2));
	// pr($data);die;

	if(!empty($data['UserProject'])){
		foreach ($data['UserProject'] as $key => $value) {
			$user_id = $value['user_id'];
			$userDetails[] = $this->__getUsersDetails($user_id);
		}
		$this->set(compact('data','userDetails'));
	}
	
	$this->set(compact('data'));
}
private function __getUsersDetails($user_id){
	$this->loadModel('Users');
	$userDetails = $this->Users->find('first', array('conditions'=>array('Users.id'=>$user_id)));
	return $userDetails;
}
public function admin_asignproject($id) {
	if(!isset($id))
	{
		$this->redirect(array('controller'=>'AdminProjects','action'=>'index','admin'=>true)); 
	}
	$this->layout= 'Admin/admin';
	$this->set('title','Asign Projects -Techchzapp Payroll');
	$project = $this->Projects->find('list',array('fields'=>array('id','projectname')));


	if(!empty($project))
	{
		$up = $this->UserProject->find('all',array('conditions'=>array('UserProject.project_id' => $id)));
		if(!empty($up)){
			foreach ($up as $val) {
				foreach ($val as $val1) {
					$ids[] = $val1['user_id'];
				}
			}
	//$query = $this->Users->find('list',array('conditions'=>array('id not IN' => $ids)));
			$data = $this->Users->find('list',array('fields'=>array('id','email'),'conditions'=>array('id not IN' => $ids)));
		}else
		{
			$data = $this->Users->find('list',array('fields'=>array('id','email')));
		}
		
//pr($data);die;
		$this->set('data',$data);
		$this->set('project',$project);
		if($this->request->is('get')){ 
			$selected =  $this->Projects->find('first',array('conditions'=>array('Projects.id'=>$id)));
			$this->request->data['UserProject']['project_id'] = $selected['Projects']['id'];
			$this->request->data['UserProject']['projectname'] = $selected['Projects']['projectname'];
		}
		if($this->request->is('post'))
		{
			$up['UserProject']['project_id']=$this->request->data['UserProject']['project_id'];
			$up['UserProject']['user_id']=$this->request->data['UserProject']['user_id'];
//pr($this->request->data);die;
			if($this->UserProject->save($up)){
				return $this->redirect(array('controller'=>'AdminProjects','action'=>'index','admin'=>true)); 
			}
		}
	}
	else
	{
		echo"No record found";
	}
}

public function admin_addproject() {
	$this->layout= 'Admin/admin';
	$this->set('title','Add new project -Techchzapp Payroll');
	if($this->request->is('post'))
	{
		if($this->Projects->save($this->request->data)){
			return $this->redirect(array('controller'=>'AdminProjects','action'=>'index','admin'=>true)); 
		}
	}
}

public function admin_editproject($id=null) {
	if(!isset($id))
	{
		$this->redirect(array('controller'=>'AdminProjects','action'=>'index','admin'=>true)); 
	}
	$this->layout= 'Admin/admin';	 
	$this->set('title','Edit Project details -Techchzapp Payroll');
	if($this->request->is('get')){ 
		$this->request->data =  $this->Projects->find('first',array('conditions'=>array('Projects.id'=>$id)));
	}
	if($this->request->is(array('post','put'))){
		$this->request->data['Projects']['start_date'] = date('Y-m-d', strtotime($this->request->data['Projects']['start_date']));
		$this->request->data['Projects']['end_date'] = date('Y-m-d', strtotime($this->request->data['Projects']['end_date']));
		// pr($this->request->data);die;
		if($this->Projects->save($this->request->data)){
			return $this->redirect(array('controller'=>'AdminProjects','action'=>'index','admin'=>true)); 
		}
	}
}

public function admin_deletedetails($id) {
	$this->layout= 'Admin/admin';

	if($this->Projects->delete($id))
	{
		return $this->redirect(array('controller'=>'AdminProjects','action'=>'index','admin'=>true));
	}
	die;
}
public function admin_remove($user_id) {
	$this->layout= 'Admin/admin';
	$data= $this->UserProject->find('first',array('conditions'=>array('UserProject.user_id'=>$user_id)));	
	if($this->UserProject->delete($data['UserProject']['id']))
	{
		return $this->redirect(array('controller'=>'AdminProjects','action'=>'index','admin'=>true));
	}
	else
	{
		echo"1";
	}
	die;
}
public function admin_checkProjectname()
{
	$this->layout= false;
	$projectname = $_REQUEST['data'];
	$record = $this->Projects->find('count',array('conditions'=>array('Projects.projectname'=>$projectname)));

	if($record>0){
		echo "1";
	}else{
		echo "0";
	}
	die;
}

}

