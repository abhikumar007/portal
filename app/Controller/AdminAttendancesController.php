<?php
/**
* Static content controller.
*
* This file will render views from views/pages/
*
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @package       app.Controller
* @since         CakePHP(tm) v 0.2.9
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/

App::uses('AppController', 'Controller');

/**
* Static content controller
*
* Override this controller by placing a copy in controllers directory of an application
*
* @package       app.Controller
* @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
*/
class AdminAttendancesController extends AppController {
	var $Helpers = array('Html','Form');

/**
* This controller does not use a model
*
* @var array
*/
public $uses = array('User','Attendance');
public $components = array('Paginator');
public $paginate = array(
	'limit' =>10,
	'order' => array(
		'Attendance.id' => 'desc'
	)
);

/**
* Displays a view
*
* @return void
* @throws NotFoundException When the view file could not be found
*	or MissingViewException in debug mode.
*/

public function admin_index() {
	$this->layout='Admin\admin';
	$this->set('title','Employees list');
	if($this->request->is('post')){
		$text = $this->request->data['User']['text'];
		$this->User->recursive = -1;
		$this->Paginator->settings = array(
			'User' => array(
			'conditions'=>array(
					'OR'=>
					array('email'=>$text, 'username'=>$text)
				),
				'limit' =>10,
				'order' => array('Attendance.id' => 'desc')
			)
		);	
		$data = $this->paginate('User');	
		$this->set('data',$data);
	}else
	{
		$this->Paginator->settings = array(
			'User' => array(

				'limit' =>10,
				'order' => array('Attendance.id' => 'desc')
			)
		);	
		$data = $this->paginate('User');	
		$this->set('data',$data);
	}
}
public function admin_viewattendance($id=null) {
	if(!isset($id))
	{
		$this->redirect(array('controller'=>'AdminAttendances','action'=>'index','admin'=>true)); 
	}
	$this->layout='Admin\admin';
	$this->set('title','View attendance');
	if($this->request->is('post')){
		$month = $this->request->data['Attendance']['month'];
		$year = $this->request->data['Attendance']['year'];
		$this->User->unbindModel(array('hasMany'=>array('Attendance')), true);
		$this->Attendance->recursive = -1;
		$this->Paginator->settings = array(
			'Attendance' => array(
				'conditions'=>array('MONTH(checkin)'=>$month,'YEAR(checkin)'=>$year,'Attendance.user_id'=>$id),
				'limit' =>10,
				'order' => array(
					'Attendance.id' => 'desc'
				)
			)
		);

		$data = $this->paginate('Attendance');
		$this->set('data',$data);
	}
	else{
		$this->Paginator->settings = array(
			'Attendance' => array(
				'conditions'=>array('Attendance.user_id'=>$id),
				'limit' =>10,
				'order' => array('Attendance.id' => 'desc')
			)
		);	
		$data = $this->paginate('Attendance');	
		$this->set('data',$data);
	}
	$data = $this->User->find('first',array('conditions'=>array('User.id'=>$id)));
	$this->set('row',$data);	
}	
public function admin_addcomment($id=null) {
	if(!isset($id))
	{
		$this->redirect(array('controller'=>'AdminAttendances','action'=>'index','admin'=>true)); 
	}
	$this->set('title','Add comment');
	$this->layout='Admin\admin';
	$data =$this->Attendance->find('first',array('condition'=>array('Attendance.id'=>$id)));	
	if($this->request->is('post'))
		{$this->Attendance->id=$id;
			if($this->Attendance->save($this->request->data))
			{

				return $this->redirect('index');			
			}
			else
			{
				echo"0";
			}
		}
	}
	public function beforeFilter() {
		parent::beforeFilter();
	}
}

