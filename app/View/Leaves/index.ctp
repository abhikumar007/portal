
<div class="content" id="con">
    <a href="#" class="closecontent"  onclick="hide('con')">&times</a> 
    <div class="content_header"><p>Leave list</p></div>
    <?php echo $this->Flash->render('positive') ?> 
    <div class="form-group">
       <?php 
       echo $this->Form->create('leave',array('label' => false,'div' => false,'id' =>'leave','method' => 'post')); 
       
       $month = array('1' =>'January','2' => 'February', '3' => 'March','4' => 'April','5' =>'May','6' =>'June','7' => 'July','8' =>'August', '9' =>'September','10' =>'October','11' =>'November','12' =>'December');
       echo $this->Form->select('month',$month,array('class' => 'form-control','placeholder'=>'Month',  'empty' => 'Select Month','id'=>'month'));

       $year = array('2018' =>'2018','2019' => '2019', '2020' => '2020',);
       echo $this->Form->select('year',$year,array('class' => 'form-control','placeholder'=>'Year','empty' => 'Select Year','id'=>'year'));

       echo $this->Form->button('Search',array('type' => 'submit','class'=>'btn-info')); 
       echo $this->Html->link('New leave request',array('controller'=>'Leaves','action'=>'newleaverqst'));
       echo $this->Form->end(); ?> 
   </div>

   <div class="table" style="overflow-x:auto;margin-top:20px;">

    <?php
    $paginator = $this->Paginator;
        //creating our table
        echo "<table>";
            // our table header, we can sort the data user the paginator sort() method!
        echo "<tr>";
            // in the sort method, ther first parameter is the same as the column name in our table
            // the second parameter is the header label we want to display in the view
        echo "<th>" . $paginator->sort('no', 'No') . "</th>";
        echo "<th>" . $paginator->sort('type', 'Type') . "</th>";
        echo "<th>" . $paginator->sort('days', 'No of days') . "</th>";
        echo "<th>" . $paginator->sort('Effective from', 'Effective from') . "</th>";
        echo "<th>" . $paginator->sort('discription', 'Description') . "</th>";
        echo "<th>" . $paginator->sort('status', 'Status') . "</th>";
        echo "<th>" . $paginator->sort('action', 'Action') . "</th>";
        echo "</tr>";
            if($row){
        $num=1;
        $num = $this->Paginator->counter('{:start}');

            // loop through the user's records
        foreach( $row as $row ){
            if($row['leave']['status']==0)
            {
                $status = "<span style='color:orange;'>Pending</span>";
            }
            elseif($row['leave']['status']==1)
            {
                $status = "<span style='color:green;'>Approved</span>";;
            }
            else{
                $status = "<span style='color:red;'>Rejected</span>";;
            }
            echo "<tr>";
            echo "<td>".$num."</td>";
            $num = $num+1;
            echo "<td>".$row['leave']['type']."</td>";
            echo "<td>".$row['leave']['days']."</td>";
            echo "<td>".$row['leave']['start_date']."</td>";
            echo "<td>".$row['leave']['discription']."</td>";          
            echo "<td>$status</td>";
           
                if($row['leave']['status']==0 && strtotime($row['leave']['start_date'])>=strtotime(date('Y-m-d')))
                {
                echo"<td>".$this->Html->link('Delete',array('Controller'=>'leaves','action'=>'deleteleave',$row['leave']['id']),array(
                    'escape' => false,
                    'confirm' => 'Are you sure you want to delete?'
                ))."</td>";   
                  
            }else{
                // echo"<td>".$this->Html->link('Delete',array('#','style'=>array('background-color:#000000')))."</td>";   
                     echo "<td>$status</td>";    
            }
            echo "</tr>";

        }

        echo "</table>";
    echo $this->element('paginate');

    }else{
   echo "<tr><td colspan='7'>No Record Found</td></tr>";
    }
    ?>

</div>

</div>

<script>

    $(document).ready(function() {

        $("#leave").validate({
            rules: {
                "data[leave][month]": {
                    required: true,    
                },
                "data[leave][year]": {
                    required: true,    
                },       

            },

            messages: {

                "data[leave][month]": {
                    required: "Please select month.",
                    
                },
                "data[leave][year]": {
                    required: "Please select year.",
                    
                },
                
                
            },


        });
    });   
                            function hide(target) {
    document.getElementById(target).style.display = 'none';
} 

</script>