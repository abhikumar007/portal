<div class="content">
  <div class="table" style="overflow-x:auto;margin-top:20px;">
  <?php
  $paginator = $this->Paginator;
  echo "<table>";
  echo "<tr>";
  echo "<th>" . $paginator->sort('no', 'No') . "</th>";
  echo "<th>" . $paginator->sort('type', 'Type') . "</th>";
  echo "<th>" . $paginator->sort('days', 'Days') . "</th>";
  echo "<th>" . $paginator->sort('start_date', 'Effective From') . "</th>";
  echo "<th>" . $paginator->sort('end_date', 'End') . "</th>";
  echo "<th>" . $paginator->sort('discription', 'Description') . "</th>";
   echo "<th>" . $paginator->sort('status', 'Status') . "</th>";

  echo "</tr>";
  if($data){

    $num=1;
    $num = $this->Paginator->counter('{:start}');
    foreach( $data as $data){
     
      echo "<tr>";
      echo"<td>".$num."</td>";
      $num=$num+1;
      echo"<td>".$data['leave']['type']."</td>";
      echo"<td>".$data['leave']['days']."</td>";
      echo"<td>".$data['leave']['start_date']."</td>";
      echo"<td>".$data['leave']['end_date']."</td>";
       echo"<td>".$data['leave']['discription']."</td>";
        echo"<td>".$data['leave']['status']."</td>";
  
      echo "</tr>";
    }
    echo "</table>";
    echo $this->element('paginate');

  }
  else{
    echo "<tr><td colspan='7'>No Record Found</td></tr>";
  }
  ?>

</div>
</div>
