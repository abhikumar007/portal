<div class="content" id="con">
    <a href="#" class="closecontent"  onclick="hide('con')">&times</a> 
  <div class="content_header"><p>New Leave Request</p></div>
  <?php echo $this->Flash->render('positive') ?>
  <div class="form-group">
    <?php
    echo $this->Form->create('leave',array('label' => false,'div' => false,'id' =>'leave','method' => 'post')); 
    echo $this->Form->create('leave');
    $Type = array('CL' =>'CL','Sick' => 'Sick', 'Function' => 'Function');
    echo $this->Form->select('type',$Type,array('class' => 'form-control','placeholder'=>'Type','empty' => 'Select Type','id'=>'type','lebel'=>'Type'));

    echo $this->Form->input('days',array('type'=>'text','placeholder'=>'Enter No of days','id'=>'days','class'=>'form-control','div'=>false,'label'=>'No of  days'));

    echo $this->Form->input('start_date',array('type'=>'text','placeholder'=>'Enter date','id'=>'date','class'=>'form-control datepicker1','div'=>false,'label'=>'Date'));

    echo $this->Form->input('discription',array('type'=>'textarea','placeholder'=>'Enter discription','id'=>'discription','class'=>'form-control','div'=>false,'label'=>'Description'));

    echo $this->Form->button('Send',array('type' => 'submit','class'=>'btn-info')); 
    echo $this->Html->link('Go back',array('controller'=>'leaves','action'=>'index'));

    echo $this->Form->end(); 
    ?> 
  </div>

  <div class="table" style="overflow-x:auto;margin-top:20px;">
    <table>
      <tr>
        <th>No.</th>
        <th>Leaves Availed</th>
        <th>Leaves left</th>
        <th>Total leaves</th>
      </tr>
      <?php
      if(count($row))
      {
        $num=1;
        
        foreach($row as $data)
        {
          echo"<tr>";
          echo"<td>".$num."</td>";
          $num++;
          echo"<td>".$data['users']['leaveavailed']."</td>";
          echo"<td>".$data['users']['leaveleft']."</td>";
          echo"<td>".$data['users']['totalleaves']."</td>";         
          echo"</tr>";


        }
      }
      else{
        echo "<tr><td colspan='6'>No Record Found</td></tr>";
      }
      ?>
    </table>
  </div>
</div>
<script>

  $(document).ready(function() {

    $("#leave").validate({
      rules: {
        "data[leave][type]": {
          required: true,    
        },
        "data[leave][days]": {
          required: true,   
          digits: true,
          maxlength:2,  
        }, 
        "data[leave][start_date]": {
          required: true,   

        }, 
        "data[leave][discription]": {
          required: true,
          maxlength:200,       
        },       

      },

      messages: {

        "data[leave][type]": {
          required: "Please select type.",

        },
        "data[leave][days]": {
          required: "Please enter days.",
          digits: "Please enter only digits",
          minlength:'Minimum Length Should Be Greater Than 2 Characters',

        },
        "data[leave][start_date]": {
          required: "Please enter date.",
          

        },
        "data[leave][discription]": {
          required: "Please enter description.",
          minlength:'Minimum Length Should Be Greater Than 200 Characters',

        },


      },


    });
    console.log(new Date());
     $('#date').datepicker({ startDate: '0d' });
  });    
                      function hide(target) {
    document.getElementById(target).style.display = 'none';
}

</script>

