
<div class="detail">
  <div class="col-lg-3">
    <p><span>Employee Name : <?php echo $row['User']['first_name']. ' ' .$row['User']['last_name'] ?></p>          
    </div>
    <div class="col-lg-3">
      <p><span>Total leaves : <?php echo $row['User']['totalleaves'] ?></p>
      </div>
      <div class="col-lg-3">
        <p><span>Leave left : <?php echo $row['User']['leaveleft'] ?></p>
        </div>
        <div class="col-lg-3">
          <p><span>Leave availed : <?php echo $row['User']['leaveavailed'] ?></p>
          </div>
        </div>
   <div class="form-group">
    <?php echo $this->Form->create('Leave',array('label' => false,'div' => false,'id' =>'Leave','method' => 'post'));
    $month = array('1' =>'January','2' => 'February', '3' => 'March','4' => 'April','5' =>'May','6' =>'June','7' => 'July','8' =>'August', '9' =>'September','10' =>'October','11' =>'November','12' =>'December');
    echo $this->Form->select('month',$month,array('class' => 'form-control','placeholder'=>'Month','empty' => 'Select Month','id'=>'month'));

    $year = array('2018' =>'2018','2019' => '2019', '2020' => '2020',);
    echo $this->Form->select('year',$year,array('class' => 'form-control','placeholder'=>'Year','empty' => 'Select Year','id'=>'year'));

    echo $this->Form->button('<i class="fa fa-search"></i>',array('type' => 'submit','class'=>'btn-info')); 
     echo $this->Html->link('Go back',array('controller'=>'AdminLeaves','action'=>'index'),array('style' =>'color:#fff;background:#4caf50;padding:7px;float:right;margin-top:10px;'));
    
    echo $this->Form->end(); ?> 
  </div>
  <div class="table" style="overflow-x:auto;margin-top:20px;">
  <?php
  $paginator = $this->Paginator;
  echo "<table>";
  echo "<tr>";
  echo "<th>" . $paginator->sort('no', 'No') . "</th>";
  echo "<th>" . $paginator->sort('type', 'Type') . "</th>";
  echo "<th>" . $paginator->sort('days', 'Days') . "</th>";
  echo "<th>" . $paginator->sort('start_date', 'Effective From') . "</th>";
  echo "<th>" . $paginator->sort('end_date', 'End') . "</th>";
  echo "<th>" . $paginator->sort('discription', 'Description') . "</th>";
   echo "<th>" . $paginator->sort('status', 'Status') . "</th>";
  echo "</tr>";
  if($data){
    $num=1;
    $num = $this->Paginator->counter('{:start}');
    foreach( $data as $data){
       if($data['Leave']['status']==0)
           {
            $status =  $this->Html->link($this->Html->tag('i ','', array('class' => 'fa fa-check')),array('admin'=>true,'controller'=>'AdminLeaves','action'=>'leaverequestaction',$data['Leave']['id'],$row['User']['id'],'Accept'), array('title'=>'Accept','escape' => false)).

            ' ' .$this->Html->link($this->Html->tag('i ','', array('class' => 'fa fa-ban')),array('admin'=>true,'controller'=>'AdminLeaves','action'=>'addcomment',$data['Leave']['id'],$row['User']['id'],'Decline'), array('title'=>'Decline','escape' => false));
            // $this->Html->link('Accept',array('admin'=>true,'controller'=>'AdminLeaves','action'=>'leaverequestaction',$data['Leave']['id'],$row['User']['id'],'Accept'),array('style' =>'color:#fff;background:#4caf50;padding:5px;'))
          }
          elseif($data['Leave']['status']==1)
          {
            $status = "<span style='color:green;'>Approved</span>";;
          }
          else{
            $status = "<span style='color:red;'>Rejected</span>";;
          }

      echo "<tr>";
      echo"<td>".$num."</td>";
      $num=$num+1;
      echo"<td>".$data['Leave']['type']."</td>";
      echo"<td>".$data['Leave']['days']."</td>";
      echo"<td>".$data['Leave']['start_date']."</td>";
      echo"<td>".$data['Leave']['end_date']."</td>";
       echo"<td>".$data['Leave']['discription']."</td>";
        echo"<td>".$status."</td>";
      echo "</tr>";
    }
    echo "</table>";
    echo $this->element('paginate');

  }
  else{
    echo "<tr><td colspan='7'>No Record Found</td></tr>";
  }
  ?>

</div>

<script>

  $(document).ready(function() {

    $("#Leave").validate({
      rules: {
        "data[Leave][month]": {
          required: true,    
        },
        "data[Leave][year]": {
          required: true,    
        },       

      },

      messages: {

        "data[Leave][month]": {
          required: "Please select month.",
          
        },
        "data[Leave][year]": {
          required: "Please select year.",
          
        },
        
        
      },


    });

  });    
  function hide(target) {
    document.getElementById(target).style.display = 'none';
  }

</script>
