<div class="form-group">
  <?php echo $this->Form->create('Leave',array('label' => false,'div' => false,'id' =>'Leave','method' => 'post')); 

  echo $this->Form->input('comment',array('type'=>'textarea','id'=>'comment','class'=>'form-control ','div'=>false,'label'=>'Comment')); 

  echo $this->Form->button('Submit',array('type' => 'submit','class'=>'btn-info')); 
  echo $this->Html->link('Go back',array('controller'=>'AdminLeaves','action'=>'index','admin'=>true),array('style' =>'color:#fff;background:red;padding:8px;float:right;'));
  echo $this->Form->end(); 
  ?> 
</div>
<script>

  $(document).ready(function() {
   $("#Leave").validate({
    rules: {
      "data[Leave][comment]": {
        required: true,    
      },
    },

    messages: {

      "data[Leave][comment]": {
        required: "Please submit comment.",
        
      },     
      
    },


  });

 });   

</script>