
  <div class="table" style="overflow-x:auto;margin-top:20px;">
  <?php
  $paginator = $this->Paginator;
  echo "<table>";
  echo "<tr>";
  echo "<th>" . $paginator->sort('no', 'No') . "</th>";
  echo "<th>" . $paginator->sort('type', 'Type') . "</th>";
  echo "<th>" . $paginator->sort('days', 'Days') . "</th>";
  echo "<th>" . $paginator->sort('start_date', 'Effective From') . "</th>";
  echo "<th>" . $paginator->sort('end_date', 'End') . "</th>";
  echo "<th>" . $paginator->sort('discription', 'Description') . "</th>";
   echo "<th>" . $paginator->sort('entity_count', 'Entity_count') . "</th>";
  echo "<th>" . $paginator->sort('user_id', 'User Id') . "</th>";
   echo "<th>" . $paginator->sort('Action', 'Action') . "</th>";
  echo "</tr>";
  if($data){

    $num=1;
    $num = $this->Paginator->counter('{:start}');
    foreach( $data as $data){
     
      echo "<tr>";
      echo"<td>".$num."</td>";
      $num=$num+1;
      echo"<td>".$data['Leave']['type']."</td>";
      echo"<td>".$data['Leave']['days']."</td>";
      echo"<td>".$data['Leave']['start_date']."</td>";
      echo"<td>".$data['Leave']['end_date']."</td>";
       echo"<td>".$data['Leave']['discription']."</td>";
        echo"<td>".$data[0]['entity_count']."</td>";
        echo"<td>".$data['Leave']['user_id']."</td>";
      echo"<td>".$this->Html->link('View Leaves',array('controller'=>'AdminLeaves','action'=>'viewleave',$data['Leave']['user_id']),array('style' =>'color:#fff;background:#4caf50;padding:7px;'))."</td>";  
      echo "</tr>";
    }
    echo "</table>";
    echo $this->element('paginate');

  }
  else{
    echo "<tr><td colspan='7'>No Record Found</td></tr>";
  }
  ?>

</div>
