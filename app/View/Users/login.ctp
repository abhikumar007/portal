<div class="Register">
    <h2>Log in</h2>
    <?php 
    echo $this->Flash->render('positive');
    echo $this->Form->create('User',array('label' => false,'div' => false,'id' =>'User','method' => 'post')); ?> 
    <div class="form-group">
        <?php 
        echo $this->Form->input('username',array('type'=>'text','placeholder'=>'Enter Username','class'=>'form-control','id'=>'username','div'=>false,'label'=>'Username'));

        echo $this->Form->input('password',array('type'=>'password','placeholder'=>'Enter Password','class'=>'form-control','id'=>'password','div'=>false,'label'=>'Password'));
?>
<div class="checkbox text-left">
                        <label>
                            <?php echo $this->Form->input('User.rememberme', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?><a href="javascript:void(0)" onclick="$(this).prev().click();">&nbsp;&nbsp;Remember me</a>
                        </label>
                    </div>
<?php

        echo $this->Form->button('Log in',array('type' => 'submit','class'=>'btn-info')); 
        echo $this->Html->link('Forgot password ?',array('controller'=>'Logins','action'=>'forgetpassword'));
        ?>

    </div>

    <?php echo $this->Form->end(); ?> 
</div>