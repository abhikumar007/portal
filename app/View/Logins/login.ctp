<div class="Register">
    <h2>Log in</h2>
    <?php 
    echo $this->Flash->render('positive');
    echo $this->Form->create('login',array('label' => false,'div' => false,'id' =>'login','method' => 'post')); ?> 
    <div class="form-group">
        <?php 
        echo $this->Form->input('username',array('type'=>'text','placeholder'=>'Enter Username','class'=>'form-control','id'=>'username','div'=>false,'label'=>'Username'));

        echo $this->Form->input('password',array('type'=>'password','placeholder'=>'Enter Password','class'=>'form-control','id'=>'password','div'=>false,'label'=>'Password'));


        echo $this->Form->button('Log in',array('type' => 'submit','class'=>'btn-info')); 
        echo $this->Html->link('Forgot password ?',array('controller'=>'Logins','action'=>'forgetpassword'));
        ?>

    </div>

    <?php echo $this->Form->end(); ?> 
</div>


<script>

    $(document).ready(function() {

        $("#login").validate({
            rules: {
                "data[login][username]": {
                    required: true,
                    maxlength: 30,
                },
                "data[login][password]": {
                    required: true,
                    maxlength: 20,

                }
            },

            messages: {

                "data[login][username]": {
                    required: "Please enter username.",
                    maxlength: 30,
                },
                "data[login][password]": {
                    required: "Please enter password.",
                    maxlength: "Maxlength should not be more than 20",
                },

            }
        });
    });    

</script>