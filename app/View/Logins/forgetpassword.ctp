<div class="Register">

    <h4 style="text-align: center;">Forgot-password</h4>


    <?php 
    echo $this->Flash->render('positive');
    echo $this->Form->create('forgetpassword',array('label' => false,'div' => false,'id' =>'forgetpassword','method' => 'post')); ?> 
    <div class="form-group">
        <?php 
        echo $this->Form->input('email',array('type'=>'text','placeholder'=>'Enter email','class'=>'form-control','id'=>'email','div'=>false,'label'=>'Enter your email'));

        echo $this->Form->button('Send',array('type' => 'submit','class'=>'btn-info')); 
        echo $this->Html->link('Go back',array('controller'=>'Logins','action'=>'login'));
        
        ?>

    </div>

    <?php echo $this->Form->end(); ?> 
</div>


<script>

    $(document).ready(function() {

       $("#forgetpassword").validate({
        rules: {
            "data[forgetpassword][email]": {
                required: true,    
                maxlength:40,
                email:true,
            },          

        },

        messages: {
           "data[forgetpassword][email]": {
            required: "Please enter email.",
            email: "please enter a valid email.",
            minlength:'Minimum Length Should Be Greater Than 40 Characters',
            
        },
    },


});
       $("#forgetpassword").trigger("reset");
   });     

</script>