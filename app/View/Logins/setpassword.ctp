<div class="Register">
  <h4 style="text-align: center;">Create new password</h4>

  <?php echo $this->Flash->render('positive') ?> 

  <div class="form-group">
    <?php echo $this->Form->create('setpassword',array('label' => false,'div' => false,'id' =>'setpassword','method' => 'post','enctype'=>'multipart/form-data', 'autocomplete'=>'off')); ?> 

    <?php echo $this->Form->input('newpassword',array('type'=>'password','placeholder'=>'Enter newpassword','class'=>'form-control','id'=>'newpassword','div'=>false,'label'=>'New-password', 'autocomplete'=>'off'));?>


    <?php echo $this->Form->input('confirmpassword',array('type'=>'password','placeholder'=>'Confirm password','class'=>'form-control','id'=>'confirmpassword','div'=>false,'label'=>'Confirm password'));?>


    <?php echo $this->Form->button('Save',array('type' => 'submit','class'=>'btn-info')); ?>

    <?php echo $this->Form->end(); ?> 
  </div>
</div>

<script>

  $(document).ready(function() {



    $("#setpassword").validate({
      rules: {
       "data[setpassword][newpassword]": {
        required: true, 
        
      },
      "data[setpassword][confirmpassword]": {
        required: true, 
        
        equalTo: "#newpassword"  , 

      },
      

    },

    messages: {
     "data[setpassword][newpassword]": {
      required: "Please enter new password.",
      
      
    },
    "data[setpassword][confirmpassword]": {
      required: "Please enter confirm password.",
      equalTo: "password didn't matched,please enter again.",
      
      
    },
    
    
    
  },


});
    $('#setpassword')[0].reset();
  });    

</script>