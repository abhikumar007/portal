 <section id="contents">
   <div class="form-group">
    <?php echo $this->Form->create('Leave',array('label' => false,'div' => false,'id' =>'Leave','method' => 'post'));
    $month = array('1' =>'January','2' => 'February', '3' => 'March','4' => 'April','5' =>'May','6' =>'June','7' => 'July','8' =>'August', '9' =>'September','10' =>'October','11' =>'November','12' =>'December');
    echo $this->Form->select('month',$month,array('class' => 'form-control','placeholder'=>'Month','empty' => 'Select Month','id'=>'month'));

    $year = array('2018' =>'2018','2019' => '2019', '2020' => '2020',);
    echo $this->Form->select('year',$year,array('class' => 'form-control','placeholder'=>'Year','empty' => 'Select Year','id'=>'year'));

    echo $this->Form->button('Search',array('type' => 'submit','class'=>'btn-info')); 
    
    echo $this->Form->end(); ?> 
  </div>


  <?php //pr($data);die;?>
  <div class="table" style="overflow-x:auto;margin-top:20px;">
    <div class="well text-center text-warning"><h2><?php echo $data['User']['first_name']. ' ' .$data['User']['last_name'] ?></h2></div>
    <table>

      <thead>
        <tr> 
        <th>S.no</th>         
          <th>Type</th>  
          <th>Days</th>  
          <th>Start date</th>  
          <th>End date</th>       
          <th>Description</th>       
          <th>Total Levaes</th>       
          <th>Leave Availed</th>       
          <th>Leave Left</th>       
          <th>Status</th>       

        </tr>  
      </thead>
      <tbody>
        <?php        
        if(count($data) > 0 )
        {
          $num=1;
          foreach ($data['Leave'] as $row)
          {
           echo"<tr>"; 
            echo"<td>".$num."</td>"; 
            $num++;       
           echo"<td>".$row['type']."</td>";
           echo"<td>".$row['days']."</td>";
           echo"<td>".$row['start_date']."</td>";
           echo"<td>".$row['end_date']."</td>";
             echo"<td>".$row['discription']."</td>";
           echo"<td>".$row['totalleaves']."</td>";
           echo"<td>".$row['leaveavailed']."</td>";
            echo"<td>".$row['leaveleft']."</td>";
             echo"<td>".$row['status']."</td>";
           echo"</tr>";
         }

       }
       else{
        echo "<tr><td colspan='2'>No Record Found</td></tr>";
      }
      ?>

    </tbody>
    <tfoot>
      <tr>
        <td colspan="3">&nbsp;</td>
      </tr>
    </tfoot>


  </table>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <?php echo $this->Form->create('attendance',array('label' => false,'div' => false,'id' =>'attendance','method' => 'post'));

      echo $this->Form->input('comment',array('class' => 'form-control','placeholder'=>'comment','empty' => 'comment','id'=>'comment'));

      echo $this->Form->button('Search',array('type' => 'submit','class'=>'btn-info')); 

      echo $this->Form->end(); ?> 
    </div>

  </div>
</div>
</section>  





<script>

  $(document).ready(function() {

    $("#attendance").validate({
      rules: {
        "data[attendance][month]": {
          required: true,    
        },
        "data[attendance][year]": {
          required: true,    
        },       

      },

      messages: {

        "data[attendance][month]": {
          required: "Please select month.",
          
        },
        "data[attendance][year]": {
          required: "Please select year.",
          
        },
        
        
      },


    });

  });    
  function hide(target) {
    document.getElementById(target).style.display = 'none';
  }

</script>
