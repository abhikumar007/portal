<?php
/**
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @package       app.View.Layouts
* @since         CakePHP(tm) v 0.10.0.1076
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html lang="en">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<head>
  <?php echo $this->Html->charset(); ?>
  <title><?php echo $title; ?></title>
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <?php
  echo $this->Html->css('cake.generic');
  echo $this->fetch('meta');
  echo $this->fetch('css');
  echo $this->fetch('script');
  echo $this->Html->css('bootstrap');
  echo $this->Html->css('bootstrap.min');
  echo $this->Html->css(' datepicker');
  echo $this->Html->css('admin/admin');
  echo $this->Html->script('jquery.min');
  echo $this->Html->script('bootstrap');
  echo $this->Html->script('bootstrap-datepicker');
  echo $this->Html->script('bootstrap.min');
  echo $this->Html->script('jquery-validate');
  echo $this->Html->script('adminjs');

  ?>
</head>
<body>
  <?php echo $this->element('navbar');
  echo $this->element('adminsidebar'); ?>
  <section id="contents"> 
    <?php  echo $this->fetch('content');   ?>
  </section>  

 
 <!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js"></script> -->
<!--   <script src='js/main.js'></script> -->

</body>
 <script>
    $('.datepicker').datepicker();    
  </script>

<!--   <script src='http://code.jquery.com/jquery-latest.js'></script> -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</html>