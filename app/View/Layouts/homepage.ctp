<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>

		<?php echo $title; ?>
	</title>
      <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<?php
	
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		echo $this->Html->css('bootstrap');
		echo $this->Html->css('bootstrap.min');
 
		echo $this->Html->css('style');
		echo $this->Html->script('jquery.min');
		echo $this->Html->script('bootstrap');
    echo $this->Html->script('bootstrap-datepicker');
		echo $this->Html->script('bootstrap.min');
    echo $this->Html->css('font-awesome');	
    echo $this->Html->css('font-awesome.min');  
    echo $this->Html->script('jquery-validate');


	?>
</head>
<body>
  
 <?php echo $this->element('sidebar'); ?>


    <div class="home"style="height:100vh">
   
     <?php echo $this->fetch('content');?>
  </div>
   <script>
  $('.datepicker').datepicker();
  </script>
<script>
function openNav()
 {
    document.getElementById("mySidenav").style.width = "300px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "";
}

</script>

</body>
</html>
