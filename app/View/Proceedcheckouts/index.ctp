<div class="content" id="con">
    <a href="#" class="closecontent"  onclick="hide('con')">&times</a> 
  <div class="content_header">
    <p>Proceed Checkout</p>
      
  </div>
  <?php echo $this->Flash->render('positive') ?> 
  <div class="form-group">
    <?php 

    echo $this->Form->create('proceedcheckout',array('label' => false,'div' => false,'id' =>'proceedcheckout','method' => 'post')); 
 echo $this->Form->input('id',array('type'=>'hidden','id'=>'id'));
    echo $this->Form->input('hour',array('type'=>'text','id'=>'hour','class'=>'form-control ','div'=>false,'label'=>'Worked hour'));

    echo $this->Form->input('projectname',array('type'=>'text','id'=>'projectname','class'=>'form-control ','div'=>false,'label'=>'Project name'));
    echo $this->Form->input('comment',array('type'=>'textarea','id'=>'comment','class'=>'form-control ','div'=>false,'label'=>'Description'));

    echo $this->Form->button('Save',array('type' => 'submit','class'=>'btn-info')); 
    echo $this->Html->link('Go back',array('controller'=>'Users','action'=>'index'));
     echo $this->Html->link('Proceed Checkout',array('controller'=>'Logins','action'=>'logout'), array('style' =>'float:right;margin:10px;'));

    echo $this->Form->end();

    ?> 

  </div>
<div class="table" style="overflow-x:auto;margin-top:20px;">
    <table>
        <tr>
            <th>Project name.</th>
            <th>Date</th>
             <th>Hour</th>      
            <th>Description</th>
            <th>Action</th>
        </tr>
       


        <?php
          if($row){
            foreach($row as $data)
            {
                echo"<tr>";
                echo"<td>".$data['projectname']."</td>";
                echo"<td>".$data['date']."</td>";
                echo"<td>".$data['hour']."</td>";
                echo"<td>".$data['comment']."</td>";
                 echo"<td>".$this->Html->link('Edit',array('Controller'=>'proceedcheckout','action'=>'index',$row['proceedcheckout']['id']),array(
                    'escape' => false,
                    'confirm' => 'Are you sure, you want to edit?'
                ))."</td>";     

                echo"</tr>";
            }
          }else
          {
            echo "<tr><td colspan='5'>No Record Found</td></tr>";
          }
        ?>
    </table>
</div>
</div>



<script>

  $(document).ready(function() {

    $("#proceedcheckout").validate({
      rules: {
        "data[proceedcheckout][hour]": {
          required: true, 
          digits: true,
          maxlength:2,    
        },
        "data[proceedcheckout][projectname]": {
          required: true,    
          maxlength:25,
        },
        "data[proceedcheckout][comment]": {
          required: true,
          maxlength:200,    
        },

      },

      messages: {

        "data[proceedcheckout][hour]": {
          required: "Please enter worked hour.",
          digits: "Please enter only digits",
          minlength:'Minimum Length Should Be Greater Than 25 Characters',

        },
        "data[proceedcheckout][projectname]": {
          required: "Please enter project name.",
          minlength:'Minimum Length Should Be Greater Than 25 Characters',

        },
        "data[proceedcheckout][comment]": {
          required: "Please enter description.",
          minlength:'Minimum Length Should Be Greater Than 200 Characters',

        },



      }
    });
  });    
                        function hide(target) {
    document.getElementById(target).style.display = 'none';
}
</script>