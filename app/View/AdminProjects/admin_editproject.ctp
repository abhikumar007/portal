<div class="form-group">
  <?php echo $this->Flash->render('positive') ?> 
  <?php echo $this->Form->create('Projects',array('label' => false,'div' => false,'id' =>'Projects','method' => 'put','enctype'=>'multipart/form-data')); 
  echo $this->Form->input('id',array('type'=>'hidden',  'div'=>false,'label'=>'Project name'));
  echo $this->Form->input('projectname',array('type'=>'text','placeholder'=>'Enter Projectname','class'=>'form-control',  'div'=>false,'label'=>'Project name'));
  echo $this->Form->input('projectdetails',array('type'=>'text','placeholder'=>'Enter Project details','class'=>'form-control',  'div'=>false,'label'=>'Project details'));
    echo $this->Form->input('clientname',array('type'=>'text','placeholder'=>'Enter Client Name','class'=>'form-control',  'div'=>false,'label'=>'Client name'));

     echo $this->Form->input('budget',array('type'=>'text','placeholder'=>'Enter Budget','class'=>'form-control',  'div'=>false,'label'=>'Budget'));

      echo $this->Form->input('duration',array('type'=>'text','placeholder'=>'Enter Duration','class'=>'form-control',  'div'=>false,'label'=>'Duration'));

        echo $this->Form->input('project_manager',array('type'=>'text','placeholder'=>'Enter Project Manager','class'=>'form-control',  'div'=>false,'label'=>'Project Manager'));

  echo $this->Form->input('start_date',array('type'=>'text','placeholder'=>'Enter start date','class'=>'form-control datepicker','id'=>'start_date','div'=>false,'label'=>'Start date'));
  echo $this->Form->input('end_date',array('type'=>'text','placeholder'=>'Enter end date','class'=>'form-control datepicker','id'=>'end_date','div'=>false,'label'=>'End date'));
 
  echo $this->Form->button('Save',array('type' => 'submit','class'=>'btn-info')); 
  echo $this->Html->link('Go back',array('controller'=>'AdminProjects','action'=>'index'),array('style' =>'color:#fff;background:#4caf50;padding:7px;float:right;margin:10px;'));
  echo $this->Form->end();
  ?> 
</div>
<script>
  $(document).ready(function() {
    $("#Projects").validate({
      rules: {
        "data[Projects][projectname]": {
          required: true, 
          maxlength: 25, 
        },
        "data[Projects][projectdetails]": {
          required: true, 
          maxlength: 100, 
        },
        "data[Projects][start_date]": {
          required: true,
        },
        "data[Projects][end_date]": {
          required: true, 
          maxlength: 15,    
        },
        "data[Projects][project_manager]": {
          required: true, 
          maxlength: 25,    
        },
         "data[Projects][clientname]": {
          required: true, 
          maxlength: 25,    
        },
         "data[Projects][duration]": {
          required: true, 
          maxlength: 25,    
        },
         "data[Projects][budget]": {
          required: true, 
          maxlength: 25,    
        },
      },
      messages: {
        "data[Projects][projectname]": {
          required: "Please enter project name.",
          minlength:'Minimum Length Should Be Greater Than 25 Characters',
        },
        "data[Projects][projectdetails]": {
          required: "Please enter project details.",
          minlength:'Minimum Length Should Be Greater Than 100 Characters',
        },
        "data[Projects][start_date]": {
          required: "Please enter start date.",           
        },
        "data[Projects][end_date]": {
          required: "Please enter end date.",       
        },
        "data[Projects][project_manager]": {
          required: "Please enter project manager name.",
          minlength:'Minimum Length Should Be Greater Than 25 Characters',           
        },
         "data[Projects][clientname]": {
          required: "Please enter client name.",
          minlength:'Minimum Length Should Be Greater Than 25 Characters',           
        },
          "data[Projects][duration]": {
          required: "Please enter duration.",
          minlength:'Minimum Length Should Be Greater Than 25 Characters',           
        },
          "data[Projects][budget]": {
          required: "Please enter budget.",
          minlength:'Minimum Length Should Be Greater Than 25 Characters',           
        },
      }
    });
  });
</script>