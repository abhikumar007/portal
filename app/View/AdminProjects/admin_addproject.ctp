  <div class="form-group">
    <?php echo $this->Flash->render('positive') ?> 
    <?php echo $this->Form->create('Projects',array('label' => false,'div' => false,'id' =>'Projects','method' => 'put','enctype'=>'multipart/form-data')); 

    echo $this->Form->input('projectname',array('type'=>'text','placeholder'=>'Enter Projectname','class'=>'form-control projectname',  'div'=>false,'label'=>'Project name'));
     echo "<span class='error alreadyexist' style='margin-left:20px;'></span>";
      echo $this->Form->input('projectdetails',array('type'=>'text','placeholder'=>'Enter Project details','class'=>'form-control',  'div'=>false,'label'=>'Project details'));

    // echo $this->Form->input('start_date',array('type'=>'text','placeholder'=>'Enter start date','class'=>'form-control datepicker','id'=>'start_date','div'=>false,'label'=>'Start date'));

    //     echo $this->Form->input('end_date',array('type'=>'text','placeholder'=>'Enter end date','class'=>'form-control datepicker','id'=>'end_date','div'=>false,'label'=>'End date')); 

    echo $this->Form->button('Save',array('type' => 'submit','class'=>'btn-info')); 
    echo $this->Html->link('Go back',array('controller'=>'Users','action'=>'index'),array('style' =>'color:#fff;background:red;padding:7px;float:right;margin:10px;'));
    ?>
    <?php echo $this->Form->end(); ?> 
  </div>


  <script>

    $(document).ready(function() {

      $("#Projects").validate({
        rules: {
          "data[Projects][projectname]": {
            required: true, 
            maxlength: 25, 

          },
          "data[Projects][projectdetails]": {
            required: true,
            maxlength: 100,     
          },
        },

        messages: {

          "data[Projects][projectname]": {
            required: "Please enter username.",
            minlength:'Minimum Length Should Be Greater Than 25 Characters',
            
          },
          "data[Projects][projectdetails]": {
           required: "Please enter email.",
           email: "Please enter valid email.",
           minlength:'Minimum Length Should Be Greater Than 40 Characters',
           
         },        
      }
    });

    
    $(".projectname").blur(function() {

      $.ajax({
        type: "POST",
        url: 'checkProjectname',
        data: {'data':$(this).val()},
        success: function(response){
          if(response=='1'){
            $('.projectname').val('');
            $('.alreadyexist').css({"display":"block"});
            $(".alreadyexist").html("Project name already exist, Please try another name."); 
          }
          else
          {
              $('.alreadyexist').css({"display":"none"});
          }
        }
      });

    }); 


    });
    

  </script>