<div class="form-group">
  <?php echo $this->Flash->render('positive') ?> 
  <?php echo $this->Form->create('UserProject',array('label' => false,'div' => false,'id' =>'Projects','method' => 'put','enctype'=>'multipart/form-data')); 
  // echo $this->Form->input('id',array('type'=>'text',  'div'=>false,'label'=>'Project name','class'=>'form-control'));
  // echo $this->Form->input('projectname',array('type'=>'text','placeholder'=>'Enter Projectname','class'=>'form-control',  'div'=>false,'label'=>'Project name'));
  // echo $this->Form->input('projectdetails',array('type'=>'text','placeholder'=>'Enter Project details','class'=>'form-control',  'div'=>false,'label'=>'Project details'));

  // echo $this->Form->input('start_date',array('type'=>'text','placeholder'=>'Enter start date','class'=>'form-control datepicker','id'=>'start_date','div'=>false,'label'=>'Start date'));
  echo $this->Form->input('projectname',array('type'=>'text','placeholder'=>'Enter Projectname','class'=>'form-control',  'div'=>false,'label'=>'Project name'));
    echo $this->Form->input('project_id',array('type'=>'hidden'));

    echo $this->Form->label('Assign Project');
    //echo $this->Form->select('project_id',$project,array("escape"=>false,"empty"=>"select",'type'=>'select', "class"=>"form-control"));

    echo $this->Form->label('Assign User');
    echo $this->Form->select('user_id',$data,array("escape"=>false,"empty"=>"select",'type'=>'select', "class"=>"form-control"));
 


 
  echo $this->Form->button('Save',array('type' => 'submit','class'=>'btn-info')); 
  echo $this->Html->link('Go back',array('controller'=>'AdminProjects','action'=>'index'),array('style' =>'color:#fff;background:red;padding:7px;float:right;margin:10px;'));
  echo $this->Form->end();
  ?> 
</div>
<script>
  $(document).ready(function() {
    $("#Projects").validate({
      rules: {
        "data[Projects][projectname]": {
          required: true, 
          maxlength: 25, 
        },
        "data[Projects][projectdetails]": {
          required: true, 
          maxlength: 100, 
        },
        "data[Projects][start_date]": {
          required: true,
        },

      },
      messages: {
        "data[Projects][projectname]": {
          required: "Please enter project name.",
          minlength:'Minimum Length Should Be Greater Than 25 Characters',
        },
        "data[Projects][projectdetails]": {
          required: "Please enter project details.",
          minlength:'Minimum Length Should Be Greater Than 100 Characters',
        },
        "data[Projects][start_date]": {
          required: "Please enter start date.",           
        },

      }
    });
  });
</script>