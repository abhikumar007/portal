<?php
echo $this->Html->link('Add Project',array('controller'=>'AdminProjects','action'=>'addproject'),array('style' =>'color:#fff;background:#4caf50;padding:7px;float:right;margin:10px;'));
?>
<div class="table" style="overflow-x:auto;margin-top:20px;">
  <?php
  $paginator = $this->Paginator;
  echo "<table>";
  echo "<tr>";
  echo "<th>" . $paginator->sort('no', 'No') . "</th>";
  echo "<th>" . $paginator->sort('Projectname', 'Project name') . "</th>";
  echo "<th>" . $paginator->sort('Projectdetails', 'Projectdetails') . "</th>";
  echo "<th>" . $paginator->sort('Startdate', 'Start date') . "</th>";
  echo "<th>" . $paginator->sort('End date', 'End Date') . "</th>";
  echo "<th>" . $paginator->sort('Status', 'Status') . "</th>";
  echo "<th>" . $paginator->sort('Action', 'Action') . "</th>";
  if($data){
    $num=1;
    $num = $this->Paginator->counter('{:start}');
    foreach( $data as $data){
      if($data['Projects']['status']==0)
      {
        $status = "<span style='color:#888  ;'>Pending</span>";;
      }
      elseif($data['Projects']['status']==1)
      {
        $status = "<span style='color:green;'>Running</span>";;
      }
      elseif($data['Projects']['status']==2)
      {
        $status = "<span style='color:blue;'>complete</span>";
      }else
      {
        $status = "<span style='color:green;'>Finish</span>";;
      }
      echo "<tr>";
      echo"<td>".$num."</td>";
      $num=$num+1;
      echo"<td>".$data['Projects']['projectname']."</td>";
      echo"<td>".$data['Projects']['projectdetails']."</td>";
      echo"<td>".$data['Projects']['start_date']."</td>";
      echo"<td>".$data['Projects']['end_date']."</td>";
      echo"<td>".$status."</td>";
      // echo"<td>".$this->Html->link('View',array('controller'=>'AdminProjects','action'=>'viewprojectdetails',$data['Projects']['id']),array('style' =>'color:#fff;background:#4caf50;padding:5px;')).
      // '  '.$this->Html->link('Edit',array('controller'=>'AdminProjects','action'=>'editproject',$data['Projects']['id']),array('style' =>'color:#fff;background:#4caf50;padding:5px;')).
      // // '  '.$this->Html->link('Delete',array('controller'=>'AdminProjects','action'=>'deletedetails',$data['Projects']['id']),array('style' =>'color:#fff;background:red;padding:5px;')).
      // "</td>";      


 echo"<td>".  
      $this->Html->link($this->Html->tag('i ','', array('class' => 'fa fa-eye')),array('controller'=>'AdminProjects','action'=>'viewprojectdetails',$data['Projects']['id']), array('escape' => false)).
       $this->Html->link($this->Html->tag('i ','', array('class' => 'fa fa-edit')),array('controller'=>'AdminProjects','action'=>'editproject',$data['Projects']['id']), array('escape' => false)).
       // $this->Html->link($this->Html->tag('i ','', array('class' => 'fa fa-trash')),array('controller'=>'AdminProjects','action'=>'deletedetails',$data['Projects']['id']), array('escape' => false)).
       "</td>";  


      echo "</tr>";
    }
    echo "</table>";
    echo $this->element('paginate');
  }
  else{
    echo "<tr><td colspan='8'>No Record Found</td></tr>";
  }
  ?>  
</div>
