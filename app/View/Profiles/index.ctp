<div class="content" id="con">
    <a href="#" class="closecontent"  onclick="hide('con')">&times</a> 
    <div class="content_header">
      <p>User Profile</p>
  </div>
  
  <?php echo $this->Flash->render('positive') ?> 
  <div class="form-group">
    <?php echo $this->Form->create('User',array('label' => false,'div' => false,'id' =>'User','method' => 'put','enctype'=>'multipart/form-data')); 

    echo $this->Form->input('id',array('type'=>'hidden','placeholder'=>'Enter id','class'=>'form-control','div'=>false,'label'=>'id'));

    echo $this->Form->input('username',array('type'=>'text','placeholder'=>'Enter Username','class'=>'form-control',  'div'=>false,'label'=>'Username'));

    echo $this->Form->input('email',array('type'=>'text','placeholder'=>'Enter email','class'=>'form-control','id'=>'email','div'=>false,'label'=>'Email'));



    echo $this->Form->input('first_name',array('type'=>'text','placeholder'=>'Enter Firstname','class'=>'form-control','id'=>'firstname','div'=>false,'label'=>'Firstname'));


    echo $this->Form->input('last_name',array('type'=>'text','placeholder'=>'Enter Lastname','class'=>'form-control','id'=>'lastname','div'=>false,'label'=>'Lastname'));

    echo $this->Form->input('profile_pic',array( 'type' => 'file','id'=>'file'));
    if(!empty($this->request->data['User']['profile_pic']))
    {
      echo $this->Html->image($this->request->data['User']['profile_pic'], array('height' => '100%', 'width' => '100%','escape' => false)); 
  }
  else
  {
    echo $this->Html->image('default.png', array('height' => '100%', 'width' => '100%','escape' => false)); 
}
echo" <br>";

echo $this->Form->button('Update',array('type' => 'submit','class'=>'btn-info')); 
 echo $this->Html->link('Go back',array('controller'=>'Users','action'=>'index'));
?>

</div>

<?php echo $this->Form->end(); ?> 


<div class="content_header">
  <p>Update Password</p>
</div>
<?php echo $this->Flash->render('positive') ?> 

<div class="form-group">
    <?php echo $this->Form->create('passwordupdate',array('label' => false,'div' => false,'id' =>'passwordupdate','method' => 'post','enctype'=>'multipart/form-data')); ?> 
    <?php echo $this->Form->input('oldpassword',array('type'=>'password','placeholder'=>'Enter oldpassword','class'=>'form-control','id'=>'oldpassword','div'=>false,'label'=>'Old-password'));?>

    <?php echo $this->Form->input('newpassword',array('type'=>'password','placeholder'=>'Enter newpassword','class'=>'form-control','id'=>'newpassword','div'=>false,'label'=>'New-password'));?>
    
    
    <?php echo $this->Form->input('confirmpassword',array('type'=>'password','placeholder'=>'Confirm password','class'=>'form-control','id'=>'confirmpassword','div'=>false,'label'=>'Confirm password'));?>


    <?php echo $this->Form->button('Update Password',array('type' => 'submit','class'=>'btn-info')); ?>

    <?php echo $this->Form->end(); ?> 
</div>

</div>
<script>

    $(document).ready(function() {

        $("#user").validate({
            rules: {
                "data[User][username]": {
                    required: true, 
                    maxlength: 25, 

                },
                "data[User][email]": {
                    required: true,
                    email: true,
                    maxlength: 40,     
                },
                "data[User][first_name]": {
                    required: true, 
                    maxlength: 15,    
                },
                "data[User][last_name]": {
                    required:true,    
                },
                "data[User][profile_pic]": {
                    required:true,
                    accept: "jpg|jpeg|png|gif", 
                },
            },

            messages: {

                "data[user][username]": {
                    required: "Please enter username.",
                    minlength:'Minimum Length Should Be Greater Than 25 Characters',
                    
                },
                "data[user][email]": {
                   required: "Please enter email.",
                   email: "Please enter valid email.",
                   minlength:'Minimum Length Should Be Greater Than 40 Characters',
                   
               },
               "data[user][first_name]": {
                required: "Please enter first name.",
                minlength:'Minimum Length Should Be Greater Than 10 Characters',
                
            },
            "data[user][last_name]": {
                required: "Please enter last name.",
                minlength:'Minimum Length Should Be Greater Than 15 Characters',
            },
            "data[User][profile_pic]": {
              required: "Please enter last name.",
              accept: "Not an image!",  
          },
      }
  });


        $("#passwordupdate").validate({
            rules: {
                "data[passwordupdate][oldpassword]": {
                    required: true,    
                    minlength:6,
                },
                "data[passwordupdate][newpassword]": {
                    required: true, 
                    minlength:6,   
                },
                "data[passwordupdate][confirmpassword]": {
                    required: true, 
                    minlength:6,
                    equalTo: "#newpassword"  , 

                },
                

            },

            messages: {

                
               "data[passwordupdate][oldpassword]": {
                required: "Please enter old password.",
                minlength:'Minimum Length Should Be Greater Than 6 Characters',
                
            },
            "data[passwordupdate][newpassword]": {
                required: "Please enter new password.",
                minlength:'Minimum Length Should Be Greater Than 6 Characters',
                
            },
            "data[passwordupdate][confirmpassword]": {
                required: "Please enter confirm password.",
                equalTo: "password didn't matched,please enter again.",
                minlength:'Minimum Length Should Be Greater Than 6 Characters',
                
            },
            
            
            
        },


    });
        $("#passwordupdate").trigger("reset");

    });    
            function hide(target) {
    document.getElementById(target).style.display = 'none';
}

</script>