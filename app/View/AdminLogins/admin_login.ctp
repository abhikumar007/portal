<div class="Register">
    <h2>Admin Log in</h2>
    <?php 
    echo $this->Flash->render('positive');
    echo $this->Form->create('Admin',array('label' => false,'div' => false,'id' =>'Admin','method' => 'post')); ?> 
    <div class="form-group">
        <?php 
        echo $this->Form->input('username',array('type'=>'text','placeholder'=>'Enter Username','class'=>'form-control','id'=>'username','div'=>false,'label'=>'Username'));

        echo $this->Form->input('password',array('type'=>'password','placeholder'=>'Enter Password','class'=>'form-control','id'=>'password','div'=>false,'label'=>'Password'));


        echo $this->Form->button('Log in',array('type' => 'submit','class'=>'btn-info')); 
        echo $this->Html->link('Forgot password ?',array('controller'=>'AdminLogins','action'=>'forgotpassword'));
        ?>

    </div>

    <?php echo $this->Form->end(); ?> 
</div>


<script>

    $(document).ready(function() {

        $("#Admin").validate({
            rules: {
                "data[Admin][username]": {
                    required: true,
                    maxlength: 30,
                },
                "data[Admin][password]": {
                    required: true,
                    maxlength: 20,

                }
            },

            messages: {

                "data[Admin][username]": {
                    required: "Please enter username.",
                    maxlength: 30,
                },
                "data[Admin][password]": {
                    required: "Please enter password.",
                    maxlength: "Maxlength should not be more than 20",
                },

            }
        });
    });    

</script>