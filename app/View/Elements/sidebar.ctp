<div id="mySidenav" class="sidenav1">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <ul class="list">
  <?Php
  
    if($this->Session->read('Auth.User.User.profile_pic'))
    {
      echo $this->Html->image($this->Session->read('Auth.User.User.profile_pic'), array('height' => '50%', 'width' => '100%','escape' => false)); 
  }
  else
  {
    echo $this->Html->image('default.png', array('height' => '100%', 'width' => '100%','escape' => false)); 
}
    ?>
    <br><br>
        <li><?php echo $this->Html->link('Attendance',array('controller'=>'Attendances','action'=>'index'));?></li> 
        <li><?php echo $this->Html->link('Holidays',array('controller'=>'Holidays','action'=>'index'));?></li>
        <li><?php echo $this->Html->link('Leave',array('controller'=>'Leaves','action'=>'index'));?></li>         
        <li><?php echo $this->Html->link('Project Report',array('controller'=>'Projectreports','action'=>'index'));?></li>
        <li><?php echo $this->Html->link('Profile',array('controller'=>'Profiles','action'=>'index'));?></li>
        <li><?php echo $this->Html->link('Infra Issues',array('controller'=>'Infraissues','action'=>'index'));?></li> 
        <li><?php echo $this->Html->link('Proceed Checkout',array('controller'=>'Proceedcheckouts','action'=>'index'));?></li>      
        <li><?php echo $this->Html->link('Logout',array('controller'=>'Logins','action'=>'logout'));?></li>    
         <li><?php 
                  if(isset($countNotificationfront)){$countNotificationfront;}else{$countNotificationfront=0;};
                        echo $this->Html->link(
                        $this->Html->tag('span',$countNotificationfront, array('class' => 'badge')) .'Leave',
                        array('controller' => 'Leaves', 'action' => 'notifications'),array( 'escape' => false));
                    ?>                      
                    </li>  
                     <li><?php 
                  if(isset($countNotificationinfraissuefront)){$countNotificationinfraissuefront;}else{$countNotificationinfraissuefront=0;};
                        echo $this->Html->link(
                        $this->Html->tag('span',$countNotificationinfraissuefront, array('class' => 'badge')) .'Infraissue',
                        array('controller' => 'Infraissues', 'action' => 'notifications'),array( 'escape' => false));
                    ?>                      
                    </li>
      
  </ul>
</div>

<div id="navbar">
 <span  style="font-size:25px;color:#fff;cursor:pointer; position: fixed; border: 2px solid #fff;border-radius: 4px;padding: 3px;margin: 10px;z-index: 9999;" onclick="openNav()">&#9776;</span>
</div>