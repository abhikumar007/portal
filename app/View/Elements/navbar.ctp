 <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <i class="fa fa-align-right"></i>
            </button>
            <a class="navbar-brand" href="#">Payroll<span class="main-color">-Admin</span></a>
          </div>
          <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
       
               <li class="dropdown">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class ='fa fa-bell-o'></i><span class="caret"></span></a>

                 <ul class="dropdown-menu">
                  <li><?php 
                  if(isset($countNotification)){$countNotification;}else{$countNotification=0;};
                        echo $this->Html->link(
                        $this->Html->tag('span',$countNotification, array('class' => 'badge')) .'Leave',
                        array('controller' => 'AdminLeaves', 'action' => 'notifications','admin'=>true),
                        array( 'escape' => false)
                    );
                    ?>                      
                    </li>

                      <li><?php 
                  if(isset($countNotificationinfraissue)){$countNotificationinfraissue;}else{$countNotificationinfraissue=0;};
                        echo $this->Html->link(
                        $this->Html->tag('span',$countNotificationinfraissue, array('class' => 'badge')) .'Infraissue',
                        array('controller' => 'AdminInfraissues', 'action' => 'notifications','admin'=>true),
                        array( 'escape' => false)
                    );
                    ?>                      
                    </li>
                 </ul>
               </li>        
              <li><a href="#"><i data-show="show-side-navigation1" class="fa fa-bars show-side-btn"></i></a></li>
            </ul>
          </div>
        </div>
      </nav>