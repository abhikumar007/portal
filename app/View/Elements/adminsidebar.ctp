    <aside class="side-nav" id="show-side-navigation1">
      <i class="fa fa-bars close-aside hidden-sm hidden-md hidden-lg" data-close="show-side-navigation1"></i>
<!--   <div class="heading">
  <?Php
  
    if($this->Session->read('Auth.Admin.Admin.profile_pic'))
    {
      echo $this->Html->image($this->Session->read('Auth.Admin.Admin.profile_pic'), array('height' => '70%', 'width' => '100%','escape' => false)); 
  }
  else
  {
    echo $this->Html->image('default.png', array('height' => '100%', 'width' => '100%','escape' => false)); 
}
    ?>
    
  </div>  --> 
  <ul>

    <li><?php echo $this->Html->link('Dashboard',array('controller'=>'Admins','action'=>'index'));?></li>
    <li><?php echo $this->Html->link('Manage Employees',array('controller'=>'Employees','action'=>'index'));?></li>
    <li><?php echo $this->Html->link('Manage Holidays',array('controller'=>'AdminHolidays','action'=>'index'));?></li>
    <li><?php echo $this->Html->link('Manage Attendance',array('controller'=>'AdminAttendances','action'=>'index'));?></li>
    <li><?php echo $this->Html->link('Manage Leave request',array('controller'=>'AdminLeaves','action'=>'manageleaverequest'));?></li>
    <li><?php echo $this->Html->link('Manage Leave',array('controller'=>'AdminLeaves','action'=>'index'));?></li>
    <li><?php echo $this->Html->link('Manage Project Report',array('controller'=>'AdminProjectreports','action'=>'index'));?></li>
    <li><?php echo $this->Html->link('Manage Infraissue',array('controller'=>'AdminInfraissues','action'=>'index'));?></li>
    <li><?php echo $this->Html->link('Manage Project list',array('controller'=>'AdminProjects','action'=>'index'));?></li>
     <li><?php echo $this->Html->link('Logout',array('controller'=>'Admins','action'=>'logout'));?></li>
   

  </ul>

    </aside>

    