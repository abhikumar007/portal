
  <div class="table" style="overflow-x:auto;margin-top:20px;">
  <?php
  $paginator = $this->Paginator;
  echo "<table>";
  echo "<tr>";
  echo "<th>" . $paginator->sort('no', 'No') . "</th>";
  echo "<th>" . $paginator->sort('title', 'Title') . "</th>";
  echo "<th>" . $paginator->sort('date', 'Date') . "</th>";
  echo "<th>" . $paginator->sort('discription', 'Description') . "</th>";
   echo "<th>" . $paginator->sort('entity_count', 'Entity_count') . "</th>";
  echo "<th>" . $paginator->sort('user_id', 'User Id') . "</th>";
   echo "<th>" . $paginator->sort('Action', 'Action') . "</th>";
  echo "</tr>";
  if($data){

    $num=1;
    $num = $this->Paginator->counter('{:start}');
    foreach( $data as $data){
     
      echo "<tr>";
      echo"<td>".$num."</td>";
      $num=$num+1;
      echo"<td>".$data['Infraissue']['title']."</td>";
      echo"<td>".$data['Infraissue']['date']."</td>";
       echo"<td>".$data['Infraissue']['discription']."</td>";
        echo"<td>".$data[0]['entity_count']."</td>";
        echo"<td>".$data['Infraissue']['user_id']."</td>";
      echo"<td>".$this->Html->link('View Leaves',array('controller'=>'AdminInfraissues','action'=>'viewinfraissue',$data['Infraissue']['user_id']),array('style' =>'color:#fff;background:#4caf50;padding:7px;'))."</td>";  
      echo "</tr>";
    }
    echo "</table>";
    echo $this->element('paginate');

  }
  else{
    echo "<tr><td colspan='7'>No Record Found</td></tr>";
  }
  ?>

</div>
