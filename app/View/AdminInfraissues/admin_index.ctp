
<div class="form-group">
  <?php echo $this->Form->create('User',array('label' => false,'div' => false,'id' =>'User','method' => 'post'));

  echo $this->Form->input('text',array('type'=>'text','id'=>'text','class'=>'form-control ','div'=>false,'label'=>'Enter Email/User name'));

 echo $this->Form->button('<i class="fa fa-search"></i>',array('type' => 'submit','class'=>'btn-info')); 

  echo $this->Form->end(); ?> 
</div>
<div class="table" style="overflow-x:auto;margin-top:20px;">
  <?php
  $paginator = $this->Paginator;
  echo "<table>";
  echo "<tr>";
  echo "<th>" . $paginator->sort('no', 'No') . "</th>";
  echo "<th>" . $paginator->sort('Username', 'Username') . "</th>";
  echo "<th>" . $paginator->sort('Firstname', 'Firstname') . "</th>";
  echo "<th>" . $paginator->sort('Lastname', 'Lastname') . "</th>";
  echo "<th>" . $paginator->sort('Email', 'Email') . "</th>";
  echo "<th>" . $paginator->sort('Action', 'Action') . "</th>";
  echo "</tr>";
  if($data){

    $num=1;
    $num = $this->Paginator->counter('{:start}');
    foreach( $data as $data){

      echo "<tr>";
      echo"<td>".$num."</td>";
      $num=$num+1;
      echo"<td>".$data['User']['username']."</td>";
      echo"<td>".$data['User']['first_name']."</td>";
      echo"<td>".$data['User']['last_name']."</td>";
      echo"<td>".$data['User']['email']."</td>";
      echo"<td>".  $this->Html->link($this->Html->tag('i ','', array('class' => 'fa fa-eye')),array('controller'=>'AdminInfraissues','action'=>'viewinfraissue',$data['User']['id']), array('escape' => false))."</td>";  


     
        echo"</tr>";
    }
    echo "</table>";
    echo $this->element('paginate');

  }
  else{
    echo "<tr><td colspan='6'>No Record Found</td></tr>";
  }
  ?>

</div>
<script>

  $(document).ready(function() {
    $("#User").validate({
      rules: {
        "data[User][text]": {
          required: true,
    
          maxlength: 40,     
        },    

      },
      messages: {
        "data[User][text]": {
          required: "Please enter username or email.",
          minlength:'Minimum Length Should Be Greater Than 40 Characters',
        },
      },
      

    });

  });  

</script>
