<div class="form-group">
  <?php
   echo $this->Form->create('Infraissue',array('label' => false,'div' => false,'id' =>'Infraissue','method' => 'post')); 
  echo $this->Form->input('addcomment',array('type'=>'textarea','id'=>'addcomment','class'=>'form-control ','div'=>false,'label'=>'Comment'));
   $status = array('0' =>'pending','1' =>'solved','2' => 'in progress');
  echo $this->Form->select('status',$status,array('class' => 'form-control','placeholder'=>'Month','empty' => 'Select status','id'=>'status'));
  echo $this->Form->button('Submit',array('type' => 'submit','class'=>'btn-info')); 
  echo $this->Html->link('Go back',array('controller'=>'AdminInfraissues','action'=>'index'),array('style' =>'color:#fff;background:#4caf50;padding:7px;float:right;margin-top:10px;'));
  echo $this->Form->end(); 
  ?> 
</div>
<script>
  $(document).ready(function() {
    $("#Infraissue").validate({
      alert("csncb");

      rules: {
        "data[Infraissue][addcomment]": {
          required: true, 
        },
        "data[Infraissue][status]": {
          required: true,  
        },
      },

      messages: {
        "data[Infraissue][addcomment]": {
          required: "Please enter comment.",
        },
        "data[Infraissue][status]": {
          required: "Please select status.",
        },
      },

    });
  });

</script>