<div class="content" id="con">
    <a href="#" class="closecontent"  onclick="hide('con')">&times</a> 
    <div class="content_header"><p>Holidays</p></div>
    <div class="form-group">
     <?php 
     echo $this->Form->create('holiday',array('label' => false,'div' => false,'id' =>'holiday','method' => 'post')); 
     $month = array('1' =>'January','2' => 'February', '3' => 'March','4' => 'April','5' =>'May','6' =>'June','7' => 'july','8' =>'August', '9' =>'September','10' =>'October','11' =>'November','12' =>'December');
     echo $this->Form->select('month',$month,array('class' => 'form-control','placeholder'=>'Month','id'=>'month','empty' => 'Select Month'));

     $year = array('2018' =>'2018','2019' => '2019', '2020' => '2020',);
     echo $this->Form->select('year',$year,array('class' => 'form-control','placeholder'=>'Year','id'=>'year','empty' => 'Select Year'));

     echo $this->Form->button('Search',array('type' => 'submit','class'=>'btn-info'));

     echo $this->Form->end(); 
     ?> 
 </div>
 <div class="table" style="overflow-x:auto;margin-top:20px;">
    <table>
        <tr>
            <th>No.</th>
            <th>Date</th>      
            <th>Description</th>
        </tr>
        <?php
        if(count($row))
        {
                $num=1;
            foreach($row as $data)
            {
                echo"<tr>";
                echo"<td>".$num."</td>";
                $num++;
                echo"<td>".$data['holiday']['date']."</td>";
                echo"<td>".$data['holiday']['discription']."</td>";

                echo"</tr>";
            }
        }
        else{
            echo "<tr><td colspan='3'>No Record Found</td></tr>";
        }
        ?>
    </table>
</div>
</div>
<script>

    $(document).ready(function() {

        $("#holiday").validate({
            rules: {
                "data[holiday][month]": {
                    required: true,    
                },
                "data[holiday][year]": {
                    required: true,    
                },       

            },

            messages: {

                "data[holiday][month]": {
                    required: "Please select month.",
                    
                },
                "data[holiday][year]": {
                    required: "Please select year.",
                    
                },
                
                
            },


        });
    });    
                      function hide(target) {
    document.getElementById(target).style.display = 'none';
}

</script>