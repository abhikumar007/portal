<div class="form-group">
  <?php echo $this->Form->create('Attendance',array('label' => false,'div' => false,'id' =>'Attendance','method' => 'post')); 

  echo $this->Form->input('comment',array('type'=>'textarea','id'=>'comment','class'=>'form-control ','div'=>false,'label'=>'Comment'));

  echo $this->Form->button('Submit',array('type' => 'submit','class'=>'btn-info')); 
  echo $this->Html->link('Go back',array('controller'=>'AdminAttendances','action'=>'index','admin'=>true),array('style' =>'color:#fff;background:red;padding:8px;float:right;'));
  echo $this->Form->end(); 
  ?> 
</div>
<script>

  $(document).ready(function() {
   $("#Attendance").validate({
    rules: {
      "data[Attendance][comment]": {
        required: true,    
      },
    },

    messages: {

      "data[Attendance][comment]": {
        required: "Please submit comment.",
        
      },     
      
    },


  });

 });   

</script>