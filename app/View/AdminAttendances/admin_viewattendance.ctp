<div class="detail">
  <div class="col-lg-4">
    <p><span>Employee Name : <?php echo $row['User']['first_name']. ' ' .$row['User']['last_name'] ?></p>          
    </div>
    <div class="col-lg-4">
      <p><span>User Id : <?php echo $row['User']['id'] ?></p>
      </div>
      <div class="col-lg-4">
        <p><span>Email : <?php echo $row['User']['email'] ?></p>
        </div>
      </div>
      <div class="form-group">
        <?php echo $this->Form->create('Attendance',array('label' => false,'div' => false,'id' =>'Attendance','method' => 'post'));
        $month = array('1' =>'January','2' => 'February', '3' => 'March','4' => 'April','5' =>'May','6' =>'June','7' => 'July','8' =>'August', '9' =>'September','10' =>'October','11' =>'November','12' =>'December');
        echo $this->Form->select('month',$month,array('class' => 'form-control','placeholder'=>'Month','empty' => 'Select Month','id'=>'month'));

        $year = array('2018' =>'2018','2019' => '2019', '2020' => '2020',);
        echo $this->Form->select('year',$year,array('class' => 'form-control','placeholder'=>'Year','empty' => 'Select Year','id'=>'year'));

       echo $this->Form->button('<i class="fa fa-search"></i>',array('type' => 'submit','class'=>'btn-info')); 
        echo $this->Html->link('Go back',array('controller'=>'AdminAttendances','action'=>'index'),array('style' =>'color:#fff;background:#4caf50;padding:7px;float:right;margin-top:10px;'));

        echo $this->Form->end(); ?> 
      </div>

      <div class="table" style="overflow-x:auto;margin-top:20px;">

        <?php
        $paginator = $this->Paginator;
        echo "<table>";
        echo "<tr>";
        echo "<th>" . $paginator->sort('no', 'No') . "</th>";
        echo "<th>" . $paginator->sort('checkin', 'checkin') . "</th>";
        echo "<th>" . $paginator->sort('checkout', 'checkout') . "</th>";
           echo "<th>" . $paginator->sort('hour', 'Hour') . "</th>";
        echo "<th>" . $paginator->sort('comment', 'comment') . "</th>";
        echo "<th>" . $paginator->sort('Action', 'Action') . "</th>";
        echo "</tr>";
        if($data){
          $num=1;
          $num = $this->Paginator->counter('{:start}');
          foreach( $data as $data){
            echo "<tr>";
            echo"<td>".$num."</td>";
            $num=$num+1;
            echo"<td>".$data['Attendance']['checkin']."</td>";
            echo"<td>".$data['Attendance']['checkout']."</td>";
             echo"<td>".$data['Attendance']['hour']."</td>";
            echo"<td>".$data['Attendance']['comment']."</td>";
           
             echo"<td>".
 $this->Html->link($this->Html->tag('i ','', array('class' => 'fa fa-comments-o')),array('controller'=>'AdminAttendances','action'=>'addcomment',$data['Attendance']['id']), array('escape' => false))."</td>"; 


            echo "</tr>";
          }
          echo "</table>";
          echo $this->element('paginate');
        }
        else{
          echo "<tr><td colspan='6'>No Record Found</td></tr>";
        }
        ?>
      </div>
      <script>

        $(document).ready(function() {

          $("#Attendance").validate({
            rules: {
              "data[Attendance][month]": {
                required: true,    
              },
              "data[Attendance][year]": {
                required: true,    
              },       

            },

            messages: {

              "data[Attendance][month]": {
                required: "Please select month.",

              },
              "data[Attendance][year]": {
                required: "Please select year.",

              },   
            },
          });

        });                                               
      </script>
