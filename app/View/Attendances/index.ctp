<div class="content" id="con">
  <a href="#" class="closecontent"  onclick="hide('con')">&times</a> 
  <div class="content_header">
    <p>View Attendance</p>
  </div>
  <div class="form-group">
    <?php echo $this->Form->create('attendance',array('label' => false,'div' => false,'id' =>'attendance','method' => 'post'));
    $month = array('1' =>'January','2' => 'February', '3' => 'March','4' => 'April','5' =>'May','6' =>'June','7' => 'July','8' =>'August', '9' =>'September','10' =>'October','11' =>'November','12' =>'December');
    echo $this->Form->select('month',$month,array('class' => 'form-control','placeholder'=>'Month','empty' => 'Select Month','id'=>'month'));

    $year = array('2018' =>'2018','2019' => '2019', '2020' => '2020',);
    echo $this->Form->select('year',$year,array('class' => 'form-control','placeholder'=>'Year','empty' => 'Select Year','id'=>'year'));

    echo $this->Form->button('Search',array('type' => 'submit','class'=>'btn-info')); 
    echo $this->Form->end(); ?> 
  </div>

  <div class="table" style="overflow-x:auto;margin-top:20px;">
    <?php
    $paginator = $this->Paginator;
//creating our table
    echo "<table>";
    echo "<tr>";
    echo "<th>" . $paginator->sort('no', 'No') . "</th>";
    echo "<th>" . $paginator->sort('Check in', 'Check in') . "</th>";
    echo "<th>" . $paginator->sort('Check out', 'Check out') . "</th>";
    echo "</tr>";
    if($row){
      $num=1;
      $num = $this->Paginator->counter('{:start}');
      foreach( $row as $row){       
        echo "<tr>";
        echo"<td>".$num."</td>";
        $num=$num+1;
        echo"<td>".$row['attendance']['checkin']."</td>";
        echo"<td>".$row['attendance']['checkout']."</td>";       
        echo "</tr>";
      }
      echo "</table>";
        echo $this->element('paginate');
    
    }
// tell the user there's no records found
    else{
     echo "<tr><td colspan='3'>No Record Found</td></tr>";
   }
   ?>
 </div>
</div>

<script>
  $(document).ready(function() {
    $("#attendance").validate({
      rules: {
        "data[attendance][month]": {
          required: true,    
        },
        "data[attendance][year]": {
          required: true,    
        },       
      },
      messages: {

        "data[attendance][month]": {
          required: "Please select month.",
        },
        "data[attendance][year]": {
          required: "Please select year.",

        },
      },
    });

  });    
                      function hide(target) {
    document.getElementById(target).style.display = 'none';
}

</script>
  