<div class="form-group">

  <?php echo $this->Flash->render('positive') ?> 
  <?php echo $this->Form->create('holiday',array('label' => false,'div' => false,'id' =>'addholiday','method' => 'put','enctype'=>'multipart/form-data')); 

  echo $this->Form->input('id',array('type'=>'hidden','placeholder'=>'Enter id','class'=>'form-control datepicker','div'=>false,'label'=>'id'));

   echo $this->Form->input('date',array('type'=>'text','placeholder'=>'Enter date','id'=>'date','class'=>'form-control datepicker1','div'=>false,'label'=>'Date'));
  echo $this->Form->input('discription',array('type'=>'text','placeholder'=>'Enter Description','class'=>'form-control',  'div'=>false,'label'=>'Description'));

  echo $this->Form->button('Update',array('type' => 'submit','class'=>'btn-info')); 
  echo $this->Html->link('Go back',array('controller'=>'AdminHolidays','action'=>'index'),array('style' =>'color:#fff;background:#4caf50;padding:7px;float:right;margin-top:10px;'));
  ?>
  <?php echo $this->Form->end(); ?> 
</div>


<script>

  $(document).ready(function() {

    $("#addholiday").validate({
      rules: {
        "data[holiday][date]": {
          required: true, 
        },
        "data[holiday][discription]": {
          required: true,
          maxlength: 40,     
        },

      },

      messages: {

        "data[holiday][date]": {
          required: "Please enter date.",

        },
        "data[holiday][discription]": {
          required: "Please enter description.",
         minlength:'Minimum Length Should Be Greater Than 40 Characters',

       },
     }
   });
     console.log(new Date());
     $('#date').datepicker({ startDate: '0d' });
  });  

</script>