<?php
echo $this->Html->link('Add New Holiday',array('controller'=>'AdminHolidays','action'=>'addholiday'),array('style' =>'color:#fff;background:#4caf50;padding:7px;float:right;margin:10px;'));
?>
<div class="form-group">
  <?php echo $this->Form->create('holiday',array('label' => false,'div' => false,'id' =>'holiday','method' => 'post'));
  $month = array('1' =>'January','2' => 'February', '3' => 'March','4' => 'April','5' =>'May','6' =>'June','7' => 'July','8' =>'August', '9' =>'September','10' =>'October','11' =>'November','12' =>'December');
  echo $this->Form->select('month',$month,array('class' => 'form-control','placeholder'=>'Month','empty' => 'Select Month','id'=>'month'));

// $year = array('2018' =>'2018','2019' => '2019', '2020' => '2020',);
// echo $this->Form->select('year',$year,array('class' => 'form-control','placeholder'=>'Year','empty' => 'Select Year','id'=>'year'));

  echo $this->Form->button('Search',array('type' => 'submit','class'=>'btn-info')); 

  echo $this->Form->end(); ?> 
</div>

<div class="table" style="overflow-x:auto;margin-top:20px;">

  <?php
  $paginator = $this->Paginator;
//creating our table
  echo "<table>";
// our table header, we can sort the data user the paginator sort() method!
  echo "<tr>";
// in the sort method, ther first parameter is the same as the column name in our table
// the second parameter is the header label we want to display in the view
  echo "<th>" . $paginator->sort('no', 'No') . "</th>";
  echo "<th>" . $paginator->sort('date', 'Date') . "</th>";
  echo "<th>" . $paginator->sort('discription', 'Description') . "</th>";
  echo "<th>" . $paginator->sort('Action', 'Action') . "</th>";
  echo "</tr>";
  if($data){

    $num=1;
    $num = $this->Paginator->counter('{:start}');
    foreach( $data as $data){

      echo "<tr>";
      echo"<td>".$num."</td>";
      $num=$num+1;
      echo"<td>".$data['holiday']['date']."</td>";
      echo"<td>".$data['holiday']['discription']."</td>";
      echo"<td>".$this->Html->link('Edit',array('controller'=>'AdminHolidays','action'=>'editholiday',$data['holiday']['id']),array('style' =>'color:#fff;background:#4caf50;padding:7px;')).'  '.$this->Html->link('Delete',array('controller'=>'AdminHolidays','action'=>'deleteholiday',$data['holiday']['id']),array('style' =>'color:#fff;background:#dd1313;padding:7px;'))."</td>";  
      echo "</tr>";
    }

    echo "</table>";
    echo $this->element('paginate');
  }
  else{
    echo "<tr><td colspan='4'>No Record Found</td></tr>";
  }
  ?>

</div>

<script>
  $(document).ready(function() {
    $("#holiday").validate({
      rules: {
        "data[holiday][month]": {
          required: true,    
        },
// "data[holiday][year]": {
//   required: true,    
// },       

},

messages: {

  "data[holiday][month]": {
    required: "Please select month.",

  },
// "data[holiday][year]": {
//   required: "Please select year.",

// },
},
});

  });    
  function hide(target) {
    document.getElementById(target).style.display = 'none';
  }

</script>