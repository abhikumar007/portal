  <div class="form-group">
    <?php echo $this->Flash->render('positive') ?> 
    <?php echo $this->Form->create('user',array('label' => false,'div' => false,'id' =>'user','method' => 'put','enctype'=>'multipart/form-data')); 

    echo $this->Form->input('username',array('type'=>'text','placeholder'=>'Enter Username','class'=>'form-control username',  'div'=>false,'label'=>'Username'));
    echo "<span class='error alreadyexist' style='margin-left:20px;'></span>";

    echo $this->Form->input('email',array('type'=>'text','placeholder'=>'Enter email','class'=>'form-control','id'=>'email','div'=>false,'label'=>'Email'));

    echo $this->Form->input('first_name',array('type'=>'text','placeholder'=>'Enter Firstname','class'=>'form-control','id'=>'firstname','div'=>false,'label'=>'Firstname'));

    echo $this->Form->input('last_name',array('type'=>'text','placeholder'=>'Enter Lastname','class'=>'form-control','id'=>'lastname','div'=>false,'label'=>'Lastname'));
    echo $this->Form->input('password',array('type'=>'text','placeholder'=>'Enter Password','class'=>'form-control','id'=>'password','div'=>false,'label'=>'Password'));

    echo $this->Form->input('profile_pic',array( 'type' => 'file','id'=>'file'));
    if(!empty($this->request->data['user']['profile_pic']))
    {
      echo $this->Html->image($this->request->data['user']['profile_pic'], array('height' => '100', 'width' => '100','escape' => false)); 
    }
    else
    {
      echo $this->Html->image('default.png', array('height' => '100', 'width' => '100','escape' => false)); 
    }
    echo" <br>";

    echo $this->Form->button('Save',array('type' => 'submit','class'=>'btn-info')); 
    echo $this->Html->link('Go back',array('controller'=>'Users','action'=>'index'));
    ?>
    <?php echo $this->Form->end(); ?> 
  </div>


  <script>

    $(document).ready(function() {

      $("#user").validate({
        rules: {
          "data[user][username]": {
            required: true, 
            maxlength: 25, 

          },
          "data[user][email]": {
            required: true,
            email: true,
            maxlength: 40,     
          },
          "data[user][first_name]": {
            required: true, 
            maxlength: 15,    
          },
          "data[user][last_name]": {
            required:true,    
          },
          "data[user][profile_pic]": {
            required:true,
            accept: "jpg|jpeg|png|gif", 
          },
          "data[user][password]": {
            required: true, 
            minlength:6, 
            
          },
        },

        messages: {

          "data[user][username]": {
            required: "Please enter username.",
            minlength:'Minimum Length Should Be Greater Than 25 Characters',
            
          },
          "data[user][email]": {
           required: "Please enter email.",
           email: "Please enter valid email.",
           minlength:'Minimum Length Should Be Greater Than 40 Characters',
           
         },
         "data[user][first_name]": {
          required: "Please enter first name.",
          minlength:'Minimum Length Should Be Greater Than 10 Characters',
          
        },
        "data[user][last_name]": {
          required: "Please enter last name.",
          minlength:'Minimum Length Should Be Greater Than 15 Characters',
        },
        "data[user][profile_pic]": {
          required: "Please select profile photo.",
          accept: "Not an image!",  
        },
        "data[user][password]": {
          required: "Please enter confirm password.",
          equalTo: "password didn't matched,please enter again.",
          minlength:'Minimum Length Should Be Greater Than 6 Characters',
          
        },
      }
    });

    
    $(".username").blur(function() {

      $.ajax({
        type: "POST",
        url: 'checkUsername',
        data: {'data':$(this).val()},
        success: function(response){
          if(response=='1'){
            $('.username').val('');
            $('.alreadyexist').css({"display":"block"});
            $(".alreadyexist").html("Username already exist, Please try another name."); 
          }
          else
          {
              $('.alreadyexist').css({"display":"none"});
          }
        }
      });

    }); 
 });
    

  </script>