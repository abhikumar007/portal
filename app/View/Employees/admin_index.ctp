
<?php
echo $this->Html->link('Add New User',array('controller'=>'Employees','action'=>'adduser'),array('style' =>'color:#fff;background:#4caf50;padding:7px;float:right;margin:10px;'));
?>

<div class="form-group">
  <?php echo $this->Form->create('user',array('label' => false,'div' => false,'id' =>'user','method' => 'post'));

  echo $this->Form->input('text',array('type'=>'text','id'=>'text','class'=>'form-control ','div'=>false,'label'=>'Enter Email/User name'));
 

  echo $this->Form->button('<i class="fa fa-search"></i>',array('type' => 'submit','class'=>'btn-info')); 

  echo $this->Form->end(); ?> 
</div>
<div class="table" style="overflow-x:auto;margin-top:20px;">
  <?php
  $paginator = $this->Paginator;
  echo "<table>";
  echo "<tr>";
  echo "<th>" . $paginator->sort('no', 'No') . "</th>";
  echo "<th>" . $paginator->sort('username', 'Username') . "</th>";
  echo "<th>" . $paginator->sort('first_name', 'Firstname') . "</th>";
  echo "<th>" . $paginator->sort('last_name', 'Lastname') . "</th>";
  echo "<th>" . $paginator->sort('email', 'Email') . "</th>";
   echo "<th>" . $paginator->sort('totalproject', 'Total Project') . "</th>";
  echo "<th>" . $paginator->sort('action', 'Action') . "</th>";
  echo "</tr>";
  if($data){

    $num=1;
    $num = $this->Paginator->counter('{:start}');
    foreach( $data as $key => $data){

      echo "<tr>";
      echo"<td>".$num."</td>";
      $num=$num+1;
      echo"<td>".$data['user']['username']."</td>";
      echo"<td>".$data['user']['first_name']."</td>";
      echo"<td>".$data['user']['last_name']."</td>";
      echo"<td>".$data['user']['email']."</td>";
      echo"<td>".$data['totalproject'][0]."</td>";
      echo"<td>".  
      $this->Html->link($this->Html->tag('i ','', array('class' => 'fa fa-eye')),array('controller'=>'Employees','action'=>'viewdetails',$data['user']['id']), array('escape' => false)).
       $this->Html->link($this->Html->tag('i ','', array('class' => 'fa fa-edit')),array('controller'=>'Employees','action'=>'editdetails',$data['user']['id']), array('escape' => false))
       // .$this->Html->link($this->Html->tag('i ','', array('class' => 'fa fa-trash')),array('controller'=>'Employees','action'=>'deletedetails',$data['user']['id']), array('escape' => false))
       ."</td>";     
      echo "</tr>";
    }
    echo "</table>";
    echo $this->element('paginate');

  }
  else{
    echo "<tr><td colspan='6'>No Record Found</td></tr>";
  }
  ?>

</div>
<script>

  $(document).ready(function() {
    $("#user").validate({
      rules: {
        "data[user][text]": {
          required: true,
    
          maxlength: 40,     
        },    

      },
      messages: {
        "data[user][text]": {
          required: "Please enter username or email.",
          minlength:'Minimum Length Should Be Greater Than 40 Characters',
        },
      },
      

    });

  });  

</script>