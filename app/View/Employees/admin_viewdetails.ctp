<div style="margin: 10px;">  <?php   echo $this->Html->link('Go back',array('controller'=>'Employees','action'=>'index'),array('style' =>'color:#fff;background:#4caf50;padding:7px;margin:5px;')); ?> 
</div>
<div class="userdetail" style="width:100%;overflow-x:auto;margin-top:10px;">
  <div class="col-lg-4">
     <table style="border:1px solid #888;">
       <tr>
     
      <td><?php  if(!empty($data['user']['profile_pic']))
    {
      echo $this->Html->image($data['user']['profile_pic'], array('height' => '150', 'width' => '200','escape' => false)); 
    }
    else
    {
      echo $this->Html->image('default.png', array('height' => '100', 'width' => '100','escape' => false)); 
    }?></td>

     
    </tr>
      <?php echo"<td>".$data['user']['username']."</td>"; ?>
  </table>

  </div>
  <div class="col-lg-8">
  <table>
    <tr>
      <th style="text-align: justify;">User id</th>
      <?php echo"<td>".$data['user']['id']."</td>"; ?>
    </tr>
   
     <tr>
      <th style="text-align: justify;">First name</th>
      <?php echo"<td>".$data['user']['first_name']."</td>"; ?>
    </tr>
     <tr>
      <th style="text-align: justify;">Last name</th>
      <?php echo"<td>".$data['user']['last_name']."</td>"; ?>
    </tr>
     <tr>
      <th style="text-align: justify;">Email</th>
      <?php echo"<td>".$data['user']['email']."</td>"; ?>
    </tr>
  

     <tr>
      <th style="text-align: justify;">Date of birth</th>
      <?php echo"<td>".$data['user']['date_of_birth']."</td>"; ?>
    </tr>

     <tr>
      <th style="text-align: justify;">Address</th>
      <?php echo"<td>".$data['user']['address']."</td>"; ?>
    </tr>

     <tr>
      <th style="text-align: justify;">Joinnng date</th>
      <?php echo"<td>".$data['user']['joinning_date']."</td>"; ?>
    </tr>

     <tr>
      <th style="text-align: justify;">salary</th>
      <?php echo"<td>".$data['user']['salary']."</td>"; ?>
    </tr>

     <tr>
      <th style="text-align: justify;">Contact number</th>
      <?php echo"<td>".$data['user']['contact']."</td>"; ?>
    </tr>

     <tr>
      <th style="text-align: justify;">Comunication Id</th>
      <?php echo"<td>".$data['user']['communication']."</td>"; ?>
    </tr>


     <tr>
      <th style="text-align: justify;">Status</th>
      <?php echo"<td>";if($data['user']['status']==1){echo "<span id='spanid' class='Userstatus' value='1' style='color:green;cursor:pointer;'>Active</span>";}else{echo "<span  id='spanid' style='color:red;cursor:pointer;' class='Userstatus' value='0'>DeActive</span>";};echo "</td>"; ?>
     </tr>
  </table>
</div>
</div>

<script>
  
  $(document).ready(function(){

    $(document).on('click', '.Userstatus', function () {
       $.ajax({
           type: "POST",
           url: '../changeStatus',
           data: {'data':$(this).html(),'id': '<?php echo $data['user']['id']?>'},
           beforeSend:function(){
             return confirm("Are you sure, you want to change the status?");
            },
           success: function(response){
               if(response=='Active'){
                
                    // if(response==0){
             $("#spanid").css({"color": "green"}); 
                

               }else{
                 $("#spanid").css({"color": "red"}); 
                 
               }
               alert('Status Changed Successfully');
               $('.Userstatus').html(response);
           }
      });


    })
  });
</script>
