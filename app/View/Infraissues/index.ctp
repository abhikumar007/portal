<div class="content" id="con">
    <a href="#" class="closecontent"  onclick="hide('con')">&times</a> 
  <div class="content_header">
    <p>Infra Issue</p>
  </div>
  <div class="form-group">
   <?php echo $this->Form->create('infraissue',array('label' => false,'div' => false,'id' =>'infraissue','method' => 'post')); 
   $month = array('1' =>'January','2' => 'February', '3' => 'March','4' => 'April','5' =>'May','6' =>'June','7' => 'July','8' =>'August', '9' =>'September','10' =>'October','11' =>'November','12' =>'December');
   echo $this->Form->select('month',$month,array('class' => 'form-control','placeholder'=>'Month','empty' => 'Select Month','id'=>'month'));

   $year = array('2018' =>'2018','2019' => '2019', '2020' => '2020',);
   echo $this->Form->select('year',$year,array('class' => 'form-control','placeholder'=>'Year','empty' => 'Select Year','id'=>'year'));

   echo $this->Form->button('Search',array('type' => 'submit','class'=>'btn-info')); 
   echo $this->Html->link('New Infra Issue',array('controller'=>'infraissues','action'=>'newinfraissue'));
   echo $this->Form->end();
   ?> 
 </div>

 <div class="table" style="overflow-x:auto;margin-top:20px;">

  <?php
  $paginator = $this->Paginator;
  

   
    //creating our table
    echo "<table>";
    
        // our table header, we can sort the data user the paginator sort() method!
    echo "<tr>";
    
            // in the sort method, ther first parameter is the same as the column name in our table
            // the second parameter is the header label we want to display in the view
    echo "<th>" . $paginator->sort('no', 'No') . "</th>";
    echo "<th>" . $paginator->sort('title', 'Title') . "</th>";
    echo "<th>" . $paginator->sort('date', 'Date') . "</th>";
    echo "<th>" . $paginator->sort('discription', 'Description') . "</th>";
    echo "<th>" . $paginator->sort('addcomment', 'Comment') . "</th>";
    echo "<th>" . $paginator->sort('status', 'Status') . "</th>";
    echo "</tr>";
      if($row){
    $num=1;
    $num = $this->Paginator->counter('{:start}');
    
        // loop through the user's records
    foreach( $row as $row ){
      if($row['infraissue']['status']==0)
           {
            $status = "<span style='color:red;'>Pending</span>";;
            
          }
           elseif($row['infraissue']['status']==1){
            $status = "<span style='color:green;'>Solved</span>";;
          }
        
          else{
            $status = "<span style='color:orange;'>In progress</span>";;
          }
      echo "<tr>";
      echo "<td>{$num}</td>";
      $num = $num+1;
      echo "<td>{$row['infraissue']['title']}</td>";
      echo "<td>{$row['infraissue']['date']}</td>";
      echo "<td>{$row['infraissue']['discription']}</td>";
       echo "<td>{$row['infraissue']['addcomment']}</td>";
        echo "<td>{$status}</td>";

      echo "</tr>";
    }
    
    echo "</table>";
    

   echo $this->element('paginate');    
  }
  
  else{
 echo "<tr><td colspan='6'>No Record Found</td></tr>";
  }
  
  ?>
  

  
</table>
</div>
</div>
<script>

  $(document).ready(function() {

    $("#infraissue").validate({
      rules: {
        "data[infraissue][month]": {
          required: true,    
        },
        "data[infraissue][year]": {
          required: true,    
        },       

      },

      messages: {

        "data[infraissue][month]": {
          required: "Please select month.",
          
        },
        "data[infraissue][year]": {
          required: "Please select year.",
          
        },
        
        
      },


    });
  });   
                        function hide(target) {
    document.getElementById(target).style.display = 'none';
} 

</script>