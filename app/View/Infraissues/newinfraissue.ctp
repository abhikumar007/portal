<div class="content" id="con">
    <a href="#" class="closecontent"  onclick="hide('con')">&times</a> 

        <div class="content_header"> <p>New Infra issues</p> </div>
        <?php echo $this->Flash->render('positive') ?> 
        <div class="form-group">
          <?php echo $this->Form->create('infraissue',array('label' => false,'div' => false,'id' =>'newinfraissue','method' => 'post')); 

          echo $this->Form->input('title',array('type'=>'text','id'=>'issue','class'=>'form-control ','div'=>false,'label'=>'Issue'));


          echo $this->Form->input('date',array('type'=>'text','placeholder'=>'Enter date','id'=>'date','class'=>'form-control datepicker','div'=>false,'label'=>'Date'));

          echo $this->Form->input('discription',array('type'=>'textarea','id'=>'discription','class'=>'form-control ','div'=>false,'label'=>'Description'));



          echo $this->Form->button('Send',array('type' => 'submit','class'=>'btn-info')); 
          echo $this->Html->link('Go back',array('controller'=>'Infraissues','action'=>'index'));


          echo $this->Form->end(); 
          ?> 
        </div>
        

        <div class="table" style="overflow-x:auto;margin-top:20px;">

          <?php
  $paginator = $this->Paginator;
            echo "<table>";
            echo "<tr>";
            echo "<th>" . $paginator->sort('no', 'No') . "</th>";
            echo "<th>" . $paginator->sort('title', 'Title') . "</th>";
            echo "<th>" . $paginator->sort('date', 'Date') . "</th>";
            echo "<th>" . $paginator->sort('discription', 'Description') . "</th>";
            echo "<th>" . $paginator->sort('status', 'Status') . "</th>";
            echo "</tr>";
               if($row){
            $num=1;
            $num = $this->Paginator->counter('{:start}');
        // loop through the user's records
            foreach( $row as $row ){
           if($row['infraissue']['status']==0)
           {
            $status = "<span style='color:red;'>Pending</span>";;
            
          }
           elseif($row['infraissue']['status']==1){
            $status = "<span style='color:green;'>Solved</span>";;
          }
        
          else{
            $status = "<span style='color:orange;'>In progress</span>";;
          }
              echo "<tr>";
              echo "<td>{$num}</td>";
              $num+=1;
              echo "<td>{$row['infraissue']['title']}</td>";
              echo "<td>{$row['infraissue']['date']}</td>";
              echo "<td>{$row['infraissue']['discription']}</td>";
              echo "<td>{$status}</td>";
              echo "</tr>";
            }
            
            echo "</table>";
            

            echo $this->element('paginate');
            
          }
          else{
          echo "<tr><td colspan='5'>No Record Found</td></tr>";
          }
          ?>

        </div>
      </div>
      <script>

        $(document).ready(function() {

          $("#newinfraissue").validate({
            rules: {
              "data[infraissue][title]": {
                required: true, 
                maxlength:40,    
              },
              "data[infraissue][date]": {
                required: true,    
              },
              "data[infraissue][discription]": {
                required: true, 
                maxlength:200,    
              },       

            },

            messages: {

              "data[infraissue][title]": {
                required: "Please enter issue",
                minlength:'Minimum Length Should Be Greater Than 20 Characters',
                
              },
              "data[infraissue][date]": {
                required: "Please select date.",
                
              },
              "data[infraissue][discription]": {
                required: "Please enter description.",
                minlength:'Minimum Length Should Be Greater Than 200 Characters',
                
              },
              
              
            },


          });
          function myFunction() {
            document.getElementById("newinfraissue").reset();
          }
        }); 

                      function hide(target) {
    document.getElementById(target).style.display = 'none';
}



      </script>
      